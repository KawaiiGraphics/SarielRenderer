#include "SarielMeshImpl.hpp"
#include "SarielRootImpl.hpp"

SarielMeshImpl::SarielMeshImpl(KawaiiMesh3D *model):
  KawaiiMeshImpl(model),
  vao(std::make_unique<QOpenGLVertexArrayObject>(this))
{
}

SarielMeshImpl::~SarielMeshImpl()
{
  preDestruct();
}

void SarielMeshImpl::draw() const
{
  if(Q_LIKELY(vao->isCreated()))
    vao->bind();
  else
    {
      const_cast<SarielMeshImpl*>(this)->bindIndices();
      const_cast<SarielMeshImpl*>(this)->bindBaseAttr();
    }
//  root()->gl().glDrawRangeElements(GL_TRIANGLES, getMinIndex(), getMaxIndex(), 3 * getModel()->getRawTriangles().size(), GL_UNSIGNED_INT, nullptr);
  root()->gl().glDrawElements(GL_TRIANGLES, 3 * getModel()->getRawTriangles().size(), GL_UNSIGNED_INT, nullptr);
}

void SarielMeshImpl::drawInstanced(size_t instances) const
{
  if(Q_LIKELY(vao->isCreated()))
    vao->bind();
  else
    {
      const_cast<SarielMeshImpl*>(this)->bindIndices();
      const_cast<SarielMeshImpl*>(this)->bindBaseAttr();
    }
  root()->gl().glDrawElementsInstanced(GL_TRIANGLES, 3 * getModel()->getRawTriangles().size(), GL_UNSIGNED_INT, nullptr, instances);
}

void SarielMeshImpl::createVertexArray()
{
  root()->taskGL([this] {
      vao->create();
      vao->bind();
      bindBaseAttr();
      bindIndices();
      vao->release();
    });
}

void SarielMeshImpl::deleteVertexArray()
{
  root()->taskGL([this] {
      if(vao->isCreated())
        {
          vao->release();
          vao->destroy();
        }
      vao.reset();
    });
}

void SarielMeshImpl::bindBaseAttr()
{
  static constexpr GLuint posInd = 0, normInd = 1, texCInd = 2;

  vao->bind();
  static_cast<SarielBufferHandle*>(getBaseAttrBuf())->bind(KawaiiBufferTarget::VBO);

  root()->gl().glEnableVertexAttribArray(posInd);
  root()->gl().glVertexAttribPointer(posInd, 4, GL_FLOAT, false, sizeof(KawaiiPoint3D), nullptr);

  root()->gl().glEnableVertexAttribArray(normInd);
  root()->gl().glVertexAttribPointer(normInd, 3, GL_FLOAT, false, sizeof(KawaiiPoint3D), (const std::byte*) sizeof(QVector4D));

  root()->gl().glEnableVertexAttribArray(texCInd);
  root()->gl().glVertexAttribPointer(texCInd, 3, GL_FLOAT, false, sizeof(KawaiiPoint3D), (const std::byte*) sizeof(QVector4D)+sizeof(QVector3D));
}

void SarielMeshImpl::bindIndices()
{
  vao->bind();
  static_cast<SarielBufferHandle*>(getIndicesBuffer())->bind(KawaiiBufferTarget::ElementArray);
}

SarielRootImpl *SarielMeshImpl::root() const
{
  return static_cast<SarielRootImpl*>(getRoot());
}
