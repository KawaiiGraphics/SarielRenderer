#ifndef SARIELMESHIMPL_HPP
#define SARIELMESHIMPL_HPP

#include <KawaiiRenderer/Geometry/KawaiiMeshImpl.hpp>
#include "../SarielRenderer_global.hpp"
#include <memory>

#include <QOpenGLVertexArrayObject>

class SarielRootImpl;
class SARIELRENDERER_SHARED_EXPORT SarielMeshImpl : public KawaiiMeshImpl
{
public:
  SarielMeshImpl(KawaiiMesh3D *model);
  ~SarielMeshImpl();

  // KawaiiMeshImpl interface
public:
  void draw() const override final;
  void drawInstanced(size_t instances) const override final;

private:
  void createVertexArray() override final;
  void deleteVertexArray() override final;
  void bindBaseAttr() override final;
  void bindIndices() override final;



  //IMPLEMENT
private:
  std::unique_ptr<QOpenGLVertexArrayObject> vao;

  SarielRootImpl* root() const;
};

#endif // SARIELMESHIMPL_HPP
