#include "KawaiiSarielFactory.hpp"

#include "SarielRootImpl.hpp"
#include "Surfaces/SarielSurfaceImpl.hpp"
#include "SarielGpuBufImpl.hpp"
#include "Geometry/SarielMeshImpl.hpp"
#include "Shaders/SarielShaderImpl.hpp"
#include "Shaders/SarielProgramImpl.hpp"
#include <KawaiiRenderer/KawaiiBufBindingImpl.hpp>
#include <KawaiiRenderer/Geometry/KawaiiMeshInstanceImpl.hpp>
#include <KawaiiRenderer/KawaiiMaterialImpl.hpp>
#include <KawaiiRenderer/Shaders/KawaiiSceneImpl.hpp>
#include "SarielCameraImpl.hpp"
#include <KawaiiRenderer/Textures/KawaiiCubemapImpl.hpp>
#include <KawaiiRenderer/Textures/KawaiiImgImpl.hpp>
#include <KawaiiRenderer/Textures/KawaiiDepthCubemapArrayImpl.hpp>
#include <KawaiiRenderer/Textures/KawaiiDepthTex2dArrayImpl.hpp>
#include "Textures/SarielEnvMapImpl.hpp"
#include "Textures/SarielFramebufferImpl.hpp"
#include <KawaiiRenderer/Textures/KawaiiTexBindingImpl.hpp>

#include <QCoreApplication>

KawaiiSarielFactory *KawaiiSarielFactory::instance = nullptr;

KawaiiSarielFactory::KawaiiSarielFactory()
{
  registerImpl<KawaiiRoot,              SarielRootImpl>              ();
  registerImpl<KawaiiSurface,           SarielSurfaceImpl>           ();
  registerImpl<KawaiiGpuBuf,            SarielGpuBufImpl>            ();
  registerImpl<KawaiiBufBinding,        KawaiiBufBindingImpl>        ();
  registerImpl<KawaiiMesh3D,            SarielMeshImpl>              ();
  registerImpl<KawaiiProgram,           SarielProgramImpl>           ();
  registerImpl<KawaiiShader,            SarielShaderImpl>            ();
  registerImpl<KawaiiMeshInstance,      KawaiiMeshInstanceImpl>      ();
  registerImpl<KawaiiMaterial,          KawaiiMaterialImpl>          ();
  registerImpl<KawaiiScene,             KawaiiSceneImpl>             ();
  registerImpl<KawaiiCamera,            SarielCameraImpl>            ();
  registerImpl<KawaiiCubemap,           KawaiiCubemapImpl>           ();
  registerImpl<KawaiiImage,             KawaiiImgImpl>               ();
  registerImpl<KawaiiDepthCubemapArray, KawaiiDepthCubemapArrayImpl> ();
  registerImpl<KawaiiDepthTex2dArray,   KawaiiDepthTex2dArrayImpl>   ();
  registerImpl<KawaiiEnvMap,            SarielEnvMapImpl>            ();
  registerImpl<KawaiiFramebuffer,       SarielFramebufferImpl>       ();
  registerImpl<KawaiiTexBinding,        KawaiiTexBindingImpl>        ();
}

KawaiiImplFactory *KawaiiSarielFactory::getInstance()
{
  if(!instance)
    createInstance();
  return instance;
}

void KawaiiSarielFactory::deleteInstance()
{
  if(instance)
    {
      instance->deleteLater();
      instance = nullptr;
    }
}

void KawaiiSarielFactory::createInstance()
{
  instance = new KawaiiSarielFactory;
  qAddPostRoutine(&KawaiiSarielFactory::deleteInstance);
}
