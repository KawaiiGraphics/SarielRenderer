#include "SarielBufferHandle.hpp"
#include "SarielRootImpl.hpp"

template class sib_utils::PairHash<GLenum, GLuint>;

SarielBufferHandle::SarielBufferHandle(const void *ptr, size_t n, SarielRootImpl &root):
  r(root),
  buf(std::make_unique<QOpenGLBuffer>()),
  data(ptr),
  length(n)
{
  r.taskGL([this] {
      buf->create();
      buf->bind();
      buf->setUsagePattern(QOpenGLBuffer::DynamicDraw);
      if(data && length)
        buf->allocate(data, length);
    });
}

SarielBufferHandle::~SarielBufferHandle()
{
  r.taskGL([this] {
      if(buf->isCreated())
        {
          buf->release();
          buf->destroy();
        }
      buf.reset();
    });
}

void SarielBufferHandle::setUboPostBindFunc(const std::function<void (GLenum, uint32_t)> &func)
{
  uboPostBind = func;
}

void SarielBufferHandle::setUboPostUnbindFunc(const std::function<void (GLenum, uint32_t)> &func)
{
  uboPostUnbind = func;
}

void SarielBufferHandle::setSubData(size_t offset, const void *ptr, size_t n)
{
  Q_ASSERT(offset + n <= length);
  r.asyncTaskGL(std::bind(&SarielBufferHandle::setSubDataWork, this, offset, ptr, n));
}

void SarielBufferHandle::setBufferData(const void *ptr, size_t n)
{
  r.asyncTaskGL(std::bind(&SarielBufferHandle::setBufferDataWork, this, ptr, n));
}

void SarielBufferHandle::sendChunkToGpu(size_t offset, size_t n)
{
  setSubData(offset, static_cast<const std::byte*>(data) + offset, n);
}

size_t SarielBufferHandle::getBufferSize() const
{
  return length;
}

void SarielBufferHandle::bind(KawaiiBufferTarget target)
{
  r.taskGL(std::bind(&SarielBufferHandle::bindWork, this, target));
}

void SarielBufferHandle::unbind(KawaiiBufferTarget target)
{
  r.taskGL(std::bind(&SarielBufferHandle::unbindWork, this, target));
}

void SarielBufferHandle::bindToBlock(KawaiiBufferTarget target, uint32_t bindingPoint)
{
  r.taskGL(std::bind(&SarielBufferHandle::bindToBlockWork, this, target, bindingPoint));
}

void SarielBufferHandle::userBinding(KawaiiBufferTarget target, uint32_t bindingPoint)
{
  bindToBlock(target, KawaiiShader::getUboCount() + 1 + bindingPoint);
}

void SarielBufferHandle::unbindFromBlock(KawaiiBufferTarget target, uint32_t bindingPoint)
{
  r.taskGL(std::bind(&SarielBufferHandle::unbindFromBlockWork, this, target, bindingPoint));
}

void SarielBufferHandle::setBufferDataWork(const void *ptr, size_t n)
{
  buf->bind();
  buf->allocate(ptr, n);
  buf->release();
  data = ptr;
  length = n;
}

void SarielBufferHandle::setSubDataWork(size_t offset, const void *ptr, size_t n)
{
  buf->bind();
  buf->write(offset, ptr, n);
  buf->release();
}

namespace {
  GLenum trBufferTarget(KawaiiBufferTarget target)
  {
    switch(target)
      {
      case KawaiiBufferTarget::UBO:
        return GL_UNIFORM_BUFFER;

      case KawaiiBufferTarget::VBO:
        return GL_ARRAY_BUFFER;

      case KawaiiBufferTarget::SSBO:
        return GL_SHADER_STORAGE_BUFFER;

      case KawaiiBufferTarget::CopyRead:
        return GL_COPY_READ_BUFFER;

      case KawaiiBufferTarget::CopyWrite:
        return GL_COPY_WRITE_BUFFER;

      case KawaiiBufferTarget::PixelPack:
        return GL_PIXEL_PACK_BUFFER;

      case KawaiiBufferTarget::PixelUnpack:
        return GL_PIXEL_UNPACK_BUFFER;

      case KawaiiBufferTarget::QueryBuffer:
        return GL_QUERY_BUFFER;

      case KawaiiBufferTarget::DrawIndirect:
        return GL_DRAW_INDIRECT_BUFFER;

      case KawaiiBufferTarget::AtomicCounter:
        return GL_ATOMIC_COUNTER_BUFFER;

      case KawaiiBufferTarget::ElementArray:
        return GL_ELEMENT_ARRAY_BUFFER;

      case KawaiiBufferTarget::DispatchIndirect:
        return GL_DISPATCH_INDIRECT_BUFFER;

      case KawaiiBufferTarget::TextureBuffer:
        return GL_TEXTURE_BUFFER;

      case KawaiiBufferTarget::TransformFeedback:
        return 0;
      }
    Q_UNREACHABLE();
  }
}

void SarielBufferHandle::bindWork(KawaiiBufferTarget target)
{
  buf->release();
  GLenum glBufTarget = trBufferTarget(target);
  if(length)
    r.gl().glBindBuffer(glBufTarget, buf->bufferId());
}

void SarielBufferHandle::unbindWork(KawaiiBufferTarget target)
{
  GLenum glBufTarget = trBufferTarget(target);
  r.gl().glBindBuffer(glBufTarget, 0);
}

void SarielBufferHandle::bindToBlockWork(KawaiiBufferTarget target, uint32_t bindingPoint)
{
  buf->release();
  GLenum glBufTarget = trBufferTarget(target);
  if(length)
    r.gl().glBindBufferBase(glBufTarget, bindingPoint, buf->bufferId());
  if(uboPostBind)
    uboPostBind(glBufTarget, bindingPoint);
}

void SarielBufferHandle::unbindFromBlockWork(KawaiiBufferTarget target, uint32_t bindingPoint)
{
  GLenum glBufTarget = trBufferTarget(target);
  r.gl().glBindBufferBase(glBufTarget, bindingPoint, 0);
  if(uboPostUnbind)
    uboPostUnbind(glBufTarget, bindingPoint);
}
