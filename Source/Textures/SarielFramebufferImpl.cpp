#include "SarielFramebufferImpl.hpp"
#include "SarielTextureHandle.hpp"
#include "SarielRootImpl.hpp"

namespace {
  GLenum trAttachementMode(KawaiiFramebuffer::AttachmentMode mode)
  {
    switch(mode)
      {
      case KawaiiFramebuffer::AttachmentMode::Depth:
        return GL_DEPTH_ATTACHMENT;

      case KawaiiFramebuffer::AttachmentMode::Color0:
        return GL_COLOR_ATTACHMENT0;

      case KawaiiFramebuffer::AttachmentMode::Color1:
        return GL_COLOR_ATTACHMENT1;

      case KawaiiFramebuffer::AttachmentMode::Color2:
        return GL_COLOR_ATTACHMENT2;

      case KawaiiFramebuffer::AttachmentMode::Color3:
        return GL_COLOR_ATTACHMENT3;

      case KawaiiFramebuffer::AttachmentMode::Color4:
        return GL_COLOR_ATTACHMENT4;

      case KawaiiFramebuffer::AttachmentMode::Color5:
        return GL_COLOR_ATTACHMENT5;

      case KawaiiFramebuffer::AttachmentMode::Color6:
        return GL_COLOR_ATTACHMENT6;

      case KawaiiFramebuffer::AttachmentMode::Color7:
        return GL_COLOR_ATTACHMENT7;

      case KawaiiFramebuffer::AttachmentMode::Stencil:
        return GL_STENCIL_ATTACHMENT;

      case KawaiiFramebuffer::AttachmentMode::DepthStencil:
        return GL_DEPTH_STENCIL_ATTACHMENT;
      }
    Q_UNREACHABLE();
  }
}

SarielFramebufferImpl::SarielFramebufferImpl(KawaiiFramebuffer *model):
  KawaiiFramebufferImpl(model),
  depthRenderbufferSz(0,0),
  fboId(0),
  depthRenderbufId(0)
{
  if(root())
    root()->taskGL([this] {
        root()->gl().glGenFramebuffers(1, &fboId);
        root()->gl().glBindFramebuffer(GL_FRAMEBUFFER, fboId);
      });
}

SarielFramebufferImpl::~SarielFramebufferImpl()
{
  preDestruct();
  if(depthRenderbufId)
    deleteRenderbuffers();
  if(fboId)
    {
      root()->taskGL([this] {
          root()->gl().glDeleteFramebuffers(1, &fboId);
        });
    }
}

void SarielFramebufferImpl::attachTexture(KawaiiTextureImpl *tex, KawaiiFramebuffer::AttachmentMode mode, int layer)
{
  root()->taskGL([this, tex, mode, layer] {
      auto texHandle = static_cast<SarielTextureHandle*>(tex->getHandle());
      texHandle->attachToFbo(fboId, trAttachementMode(mode), layer);
    });
}

void SarielFramebufferImpl::attachDepthRenderbuffer(const QSize &sz)
{
  root()->taskGL([this, &sz] {
      if(!depthRenderbufId)
        root()->gl().glGenRenderbuffers(1, &depthRenderbufId);
      root()->gl().glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbufId);

      if(depthRenderbufferSz != sz)
        {
          root()->gl().glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, sz.width(), sz.height());
          depthRenderbufferSz = sz;
        }
      root()->gl().glBindFramebuffer(GL_FRAMEBUFFER, fboId);
      root()->gl().glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbufId);
    });
}

void SarielFramebufferImpl::detachTexture(KawaiiFramebuffer::AttachmentMode mode)
{
  root()->taskGL([this, mode] {
      root()->gl().glBindFramebuffer(GL_FRAMEBUFFER, fboId);
      root()->gl().glFramebufferTexture(fboId, trAttachementMode(mode), 0, 0);
    });
}

void SarielFramebufferImpl::deleteRenderbuffers()
{
  if(depthRenderbufId)
    {
      root()->taskGL([this] {
          root()->gl().glDeleteRenderbuffers(1, &depthRenderbufId);
          depthRenderbufId = 0;
        });
    }
}

namespace {
  const std::array<float, 4> clearColor = {0,0,0,0};
  const float clearDepth = 1.0;
}

void SarielFramebufferImpl::activate()
{
  if(drawBuffers.empty())
    {
      getModel()->forallAtachments([this] (const KawaiiFramebuffer::Attachment &att) {
          if(att.mode != KawaiiFramebuffer::AttachmentMode::Depth
             && att.mode != KawaiiFramebuffer::AttachmentMode::DepthStencil)
            drawBuffers.push_back(trAttachementMode(att.mode));
        });

      if(drawBuffers.empty())
        drawBuffers.push_back(GL_NONE);
    }

  root()->gl().glBindFramebuffer(GL_FRAMEBUFFER, fboId);

  root()->gl().glDrawBuffers(drawBuffers.size(), drawBuffers.data());
  for(size_t i = 0; i < drawBuffers.size(); ++i)
    if(drawBuffers[i] != GL_NONE)
      root()->gl().glClearBufferfv(GL_COLOR, i, clearColor.data());

  root()->gl().glClearBufferfv(GL_DEPTH, 0, &clearDepth);
}

SarielRootImpl *SarielFramebufferImpl::root()
{
  return static_cast<SarielRootImpl*>(getRoot());
}
