#include "SarielEnvMapImpl.hpp"
#include <KawaiiRenderer/Textures/KawaiiTextureImpl.hpp>

namespace {
  GLenum trAttachementMode(KawaiiEnvMap::AttachmentMode mode)
  {
    switch(mode)
      {
      case KawaiiEnvMap::AttachmentMode::Color:
        return GL_COLOR_ATTACHMENT0;
      case KawaiiEnvMap::AttachmentMode::Depth:
        return GL_DEPTH_ATTACHMENT;
      case KawaiiEnvMap::AttachmentMode::DepthStencil:
        return GL_DEPTH_STENCIL_ATTACHMENT;
      case KawaiiEnvMap::AttachmentMode::Stencil:
        return GL_STENCIL_ATTACHMENT;
      }
    Q_UNREACHABLE();
  }
}

SarielEnvMapImpl::SarielEnvMapImpl(KawaiiEnvMap *model):
  KawaiiEnvMapImpl(model),
  fboId({0,0,0,0,0,0}),
  depthRenderbufferSz(0,0),
  depthRenderbufId(0),
  hasColorAttachement(false)
{
  root()->taskGL([this] {
      root()->gl().glGenFramebuffers(6, fboId.data());
      for(GLuint id: fboId)
        root()->gl().glBindFramebuffer(GL_FRAMEBUFFER, id);
    });
}

SarielEnvMapImpl::~SarielEnvMapImpl()
{
  preDestruct();
  root()->taskGL([this] {
      root()->gl().glDeleteFramebuffers(6, fboId.data());
    });
}

KawaiiEnvMapImpl::DrawCache *SarielEnvMapImpl::createDrawCache(const QRect &viewport)
{
  root()->gl().glViewport(viewport.x(), viewport.y(), viewport.width(), viewport.height());
  drawScene();
  return nullptr;
}

void SarielEnvMapImpl::attachTexture(KawaiiTextureImpl *tex, KawaiiEnvMap::AttachmentMode mode, int layer)
{
  auto texHandle = static_cast<SarielTextureHandle*>(tex->getHandle());
  if(layer < 0)
    layer = 0;
  else
    layer *= 6;

  if(!hasColorAttachement && mode == KawaiiEnvMap::AttachmentMode::Color)
    hasColorAttachement = true;

  const GLenum attMode = trAttachementMode(mode);
  root()->taskGL([this, attMode, texHandle, layer] {
      if(texHandle)
        {
          for(size_t i = 0; i < 6; ++i)
            texHandle->attachToFbo(fboId[i], attMode, layer + i);
        } else
        {
          for(GLuint id: fboId)
            {
              root()->gl().glBindFramebuffer(GL_FRAMEBUFFER, id);
              root()->gl().glFramebufferTexture(GL_FRAMEBUFFER, attMode, 0, 0);
            }
        }
    });
}

void SarielEnvMapImpl::attachDepthRenderbuffer(const QSize &sz)
{
  root()->taskGL([this, &sz] {
      if(!depthRenderbufId)
        root()->gl().glGenRenderbuffers(1, &depthRenderbufId);

      root()->gl().glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbufId);
      if(depthRenderbufferSz != sz)
        {
          root()->gl().glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, sz.width(), sz.height());
          depthRenderbufferSz = sz;
        }
      for(GLuint id: fboId)
        {
          root()->gl().glBindFramebuffer(GL_FRAMEBUFFER, id);
          root()->gl().glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbufId);
        }
    });
}

void SarielEnvMapImpl::detachTexture(KawaiiEnvMap::AttachmentMode mode)
{
  if(hasColorAttachement && mode == KawaiiEnvMap::AttachmentMode::Color)
    hasColorAttachement = false;

  const GLenum attMode = trAttachementMode(mode);
  root()->taskGL([this, attMode] {
      for(GLuint id: fboId)
        {
          root()->gl().glBindFramebuffer(GL_FRAMEBUFFER, id);
          root()->gl().glFramebufferTexture(GL_FRAMEBUFFER, attMode, 0, 0);
        }
    });
}

void SarielEnvMapImpl::deleteRenderbuffers()
{
  if(depthRenderbufId)
    {
      root()->gl().glDeleteRenderbuffers(1, &depthRenderbufId);
      depthRenderbufId = 0;
    }
}

namespace {
  const std::array<float, 4> clearColor = {0,0,0,0};
  const float clearDepth = 1.0;
}

void SarielEnvMapImpl::setRenderLayer(size_t i)
{
  root()->gl().glBindFramebuffer(GL_FRAMEBUFFER, fboId[i]);
  if(hasColorAttachement)
    {
      const GLenum colorAtt = GL_COLOR_ATTACHMENT0;
      root()->gl().glDrawBuffers(1, &colorAtt);
      root()->gl().glClearBufferfv(GL_COLOR, 0, clearColor.data());
    } else
    {
      const GLenum noneAtt = GL_NONE;
      root()->gl().glDrawBuffers(1, &noneAtt);
    }

  root()->gl().glClearBufferfv(GL_DEPTH, 0, &clearDepth);
}

SarielRootImpl *SarielEnvMapImpl::root()
{
  return static_cast<SarielRootImpl*>(getRoot());
}
