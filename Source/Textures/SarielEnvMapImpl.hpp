#ifndef SARIELENVMAPIMPL_HPP
#define SARIELENVMAPIMPL_HPP

#include <KawaiiRenderer/Textures/KawaiiEnvMapImpl.hpp>
#include "../SarielRootImpl.hpp"
#include <qopengl.h>

class SARIELRENDERER_SHARED_EXPORT SarielEnvMapImpl : public KawaiiEnvMapImpl
{
public:
  SarielEnvMapImpl(KawaiiEnvMap *model);
  ~SarielEnvMapImpl();

  // KawaiiEnvMapImpl interface
protected:
  inline void updateCache(DrawCache&, const QRect&) override final {}
  DrawCache *createDrawCache(const QRect &viewport) override final;

private:
  void attachTexture(KawaiiTextureImpl *tex, KawaiiEnvMap::AttachmentMode mode, int layer) override final;
  void attachDepthRenderbuffer(const QSize &sz) override final;
  void detachTexture(KawaiiEnvMap::AttachmentMode mode) override final;
  void deleteRenderbuffers() override final;
  void setRenderLayer(size_t i) override final;



  //IMPLEMENT
private:
  std::array<GLuint, 6> fboId;
  QSize depthRenderbufferSz;
  GLuint depthRenderbufId;

  bool hasColorAttachement;

  SarielRootImpl *root();
};

#endif // SARIELENVMAPIMPL_HPP
