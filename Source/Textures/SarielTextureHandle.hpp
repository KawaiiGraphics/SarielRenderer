#ifndef SARIELTEXTUREHANDLE_HPP
#define SARIELTEXTUREHANDLE_HPP

#include <KawaiiRenderer/Textures/KawaiiTextureHandle.hpp>
#include "../SarielRenderer_global.hpp"
#include <QOpenGLTexture>
#include <QOpenGLFramebufferObject>

class SarielRootImpl;

class SARIELRENDERER_SHARED_EXPORT SarielTextureHandle : public KawaiiTextureHandle
{
public:
  SarielTextureHandle(SarielRootImpl &root, KawaiiTexture *model);
  ~SarielTextureHandle();

  void attachToFbo(GLuint fboId, GLenum attachment, GLint layer, GLuint level = 0);
  QOpenGLTexture& getTexture();

  // KawaiiTextureHandle interface
public:
  void setData1D(int width, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr) override final;
  void setData1D(int width, int layers, KawaiiTextureFormat fmt, const float *ptr) override final;
  void setData2D(int width, int heigh, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr) override final;
  void setData2D(int width, int heigh, int layers, KawaiiTextureFormat fmt, const float *ptr) override final;
  void setData3D(int width, int heigh, int depth, KawaiiTextureFormat fmt, const uint8_t *ptr) override final;
  void setData3D(int width, int heigh, int depth, KawaiiTextureFormat fmt, const float *ptr) override final;
  void setDataCube(int width, int heigh, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr) override final;
  void setDataCube(int width, int heigh, int layers, KawaiiTextureFormat fmt, const float *ptr) override final;

  void setCompareOperation(KawaiiDepthCompareOperation op) override final;

  void setMinFilter(KawaiiTextureFilter filter) override final;
  void setMagFilter(KawaiiTextureFilter filter) override final;
  void setWrapModeS(KawaiiTextureWrapMode mode) override final;
  void setWrapModeT(KawaiiTextureWrapMode mode) override final;
  void setWrapModeR(KawaiiTextureWrapMode mode) override final;



  //IMPLEMENT
private:
  enum class StorageType: uint8_t {
    Data1D,
    Data2D,
    Data3D,
    None
  };

  SarielRootImpl &r;
  QOpenGLTexture tex;

  KawaiiTextureFormat storageFmt;
  StorageType storageType;
  bool mipmap_needed;

  bool checkMipmapIsNeeded(KawaiiTextureFilter filter) const;

  bool checkStorageCompatibility(int width, KawaiiTextureFormat fmt) const;
  bool checkStorageCompatibility(int width, int height, KawaiiTextureFormat fmt) const;
  bool checkStorageCompatibility(int width, int height, int depth, KawaiiTextureFormat fmt) const;
};

#endif // SARIELTEXTUREHANDLE_HPP
