#ifndef SARIELFRAMEBUFFERIMPL_HPP
#define SARIELFRAMEBUFFERIMPL_HPP

#include <KawaiiRenderer/Textures/KawaiiFramebufferImpl.hpp>
#include "SarielRenderer_global.hpp"
#include <qopengl.h>

class SarielRootImpl;

class SARIELRENDERER_SHARED_EXPORT SarielFramebufferImpl : public KawaiiFramebufferImpl
{
public:
  SarielFramebufferImpl(KawaiiFramebuffer *model);
  ~SarielFramebufferImpl();

  // KawaiiFramebufferImpl interface
private:
  void attachTexture(KawaiiTextureImpl *tex, KawaiiFramebuffer::AttachmentMode mode, int layer) override final;
  void attachDepthRenderbuffer(const QSize &sz) override final;
  void detachTexture(KawaiiFramebuffer::AttachmentMode mode) override final;
  void deleteRenderbuffers() override final;
  void activate() override final;



  //IMPLEMENT
private:
  std::vector<GLenum> drawBuffers;
  QSize depthRenderbufferSz;
  GLuint fboId;
  GLuint depthRenderbufId;

  SarielRootImpl *root();
};

#endif // SARIELFRAMEBUFFERIMPL_HPP
