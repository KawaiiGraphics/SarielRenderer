#include "SarielTextureHandle.hpp"
#include "SarielRootImpl.hpp"

namespace {
  QOpenGLTexture::Target trTextureType(KawaiiTextureType type)
  {
    switch (type)
      {
      case KawaiiTextureType::Texture1D:
        return QOpenGLTexture::Target1D;
      case KawaiiTextureType::Texture2D:
        return QOpenGLTexture::Target2D;
      case KawaiiTextureType::Texture3D:
        return QOpenGLTexture::Target3D;
      case KawaiiTextureType::TextureCube:
        return QOpenGLTexture::TargetCubeMap;
      case KawaiiTextureType::TextureBuffer:
        return QOpenGLTexture::TargetBuffer;
      case KawaiiTextureType::Texture1D_Array:
        return QOpenGLTexture::Target1DArray;
      case KawaiiTextureType::Texture2D_Array:
        return QOpenGLTexture::Target2DArray;
      case KawaiiTextureType::TextureRectangle:
        return QOpenGLTexture::TargetRectangle;
      case KawaiiTextureType::TextureCube_Array:
        return QOpenGLTexture::TargetCubeMapArray;
      case KawaiiTextureType::Texture2D_Multisample:
        return QOpenGLTexture::Target2DMultisample;
      case KawaiiTextureType::Texture2D_Multisample_Array:
        return QOpenGLTexture::Target2DMultisampleArray;
      }
    Q_UNREACHABLE();
  }

  QOpenGLTexture::Filter trTextureFilter(KawaiiTextureFilter filter)
  {
    switch(filter)
      {
      case KawaiiTextureFilter::Linear:
        return QOpenGLTexture::Linear;
      case KawaiiTextureFilter::Nearest:
        return QOpenGLTexture::Nearest;
      case KawaiiTextureFilter::LinearMipmapLinear:
        return QOpenGLTexture::LinearMipMapLinear;
      case KawaiiTextureFilter::LinearMipmapNearest:
        return QOpenGLTexture::LinearMipMapNearest;
      case KawaiiTextureFilter::NearestMipmapLinear:
        return QOpenGLTexture::NearestMipMapLinear;
      case KawaiiTextureFilter::NearestMipmapNearest:
        return QOpenGLTexture::LinearMipMapNearest;
      }
    Q_UNREACHABLE();
  }

  QOpenGLTexture::WrapMode trWrapMode(KawaiiTextureWrapMode mode)
  {
    switch(mode)
      {
      case KawaiiTextureWrapMode::Repeat:
        return QOpenGLTexture::Repeat;
      case KawaiiTextureWrapMode::ClampToEdge:
        return QOpenGLTexture::ClampToEdge;
      case KawaiiTextureWrapMode::ClampToBorder:
        return QOpenGLTexture::ClampToBorder;
      case KawaiiTextureWrapMode::MirroredRepeat:
        return QOpenGLTexture::MirroredRepeat;
      case KawaiiTextureWrapMode::MirroredClampToEdge:
        return QOpenGLTexture::ClampToEdge;
      }
    Q_UNREACHABLE();
  }

  QOpenGLTexture::PixelType trArgType(const uint8_t *)
  { return QOpenGLTexture::UInt8; }

  QOpenGLTexture::PixelType trArgType(const float *)
  { return QOpenGLTexture::Float32; }

  GLenum trTexGlFormat(KawaiiTextureFormat fmt)
  {
    switch(fmt)
      {
      case KawaiiTextureFormat::Red:
        return GL_RED;
      case KawaiiTextureFormat::RedGreen:
        return GL_RG;
      case KawaiiTextureFormat::RedGreenBlue:
        return GL_RGB;
      case KawaiiTextureFormat::RedGreenBlueAlpha:
        return GL_RGBA;
      case KawaiiTextureFormat::BlueGreenRed:
        return GL_BGR;
      case KawaiiTextureFormat::BlueGreenRedAlpha:
        return GL_BGRA;
      case KawaiiTextureFormat::Depth:
        return GL_DEPTH_COMPONENT;
      case KawaiiTextureFormat::DepthStencil:
        return GL_DEPTH_STENCIL;
      }
    Q_UNREACHABLE();
  }

  QOpenGLTexture::PixelFormat trTexFormat(KawaiiTextureFormat fmt)
  {
    switch(fmt)
      {
      case KawaiiTextureFormat::Red:
        return QOpenGLTexture::Red;
      case KawaiiTextureFormat::RedGreen:
        return QOpenGLTexture::RG;
      case KawaiiTextureFormat::RedGreenBlue:
        return QOpenGLTexture::RGB;
      case KawaiiTextureFormat::RedGreenBlueAlpha:
        return QOpenGLTexture::RGBA;
      case KawaiiTextureFormat::BlueGreenRed:
        return QOpenGLTexture::BGR;
      case KawaiiTextureFormat::BlueGreenRedAlpha:
        return QOpenGLTexture::BGRA;
      case KawaiiTextureFormat::Depth:
        return QOpenGLTexture::Depth;
      case KawaiiTextureFormat::DepthStencil:
        return QOpenGLTexture::DepthStencil;
      }
    Q_UNREACHABLE();
  }

  template<typename T>
  QOpenGLTexture::TextureFormat trInternalTexFormat(KawaiiTextureFormat fmt)
  {
    using T_ = sib_utils::refined_type<T>;
    if constexpr(std::is_same_v<T_, uint8_t>)
    {
      switch(fmt)
        {
        case KawaiiTextureFormat::Red:
          return QOpenGLTexture::R8_UNorm;
        case KawaiiTextureFormat::RedGreen:
          return QOpenGLTexture::RG8_UNorm;
        case KawaiiTextureFormat::RedGreenBlue:
        case KawaiiTextureFormat::BlueGreenRed:
          return QOpenGLTexture::RGB8_UNorm;
        case KawaiiTextureFormat::RedGreenBlueAlpha:
        case KawaiiTextureFormat::BlueGreenRedAlpha:
          return QOpenGLTexture::RGBA8_UNorm;
        case KawaiiTextureFormat::Depth:
          return QOpenGLTexture::D16;
        case KawaiiTextureFormat::DepthStencil:
          return QOpenGLTexture::D24S8;
        }
      Q_UNREACHABLE();
    } else if constexpr(std::is_same_v<T_, float>)
    {
      switch(fmt)
        {
        case KawaiiTextureFormat::Red:
          return QOpenGLTexture::R32F;
        case KawaiiTextureFormat::RedGreen:
          return QOpenGLTexture::RG32F;
        case KawaiiTextureFormat::RedGreenBlue:
        case KawaiiTextureFormat::BlueGreenRed:
          return QOpenGLTexture::RGB32F;
        case KawaiiTextureFormat::RedGreenBlueAlpha:
        case KawaiiTextureFormat::BlueGreenRedAlpha:
          return QOpenGLTexture::RGBA32F;
        case KawaiiTextureFormat::Depth:
          return QOpenGLTexture::D32;
        case KawaiiTextureFormat::DepthStencil:
          return QOpenGLTexture::D24S8;
        }
      Q_UNREACHABLE();
    } else
    {
      Q_UNREACHABLE();
    }
  }

  KawaiiTextureFilter filterNoMipmap(KawaiiTextureFilter filter)
  {
    switch(filter)
      {
      case KawaiiTextureFilter::LinearMipmapLinear:
      case KawaiiTextureFilter::LinearMipmapNearest:
        return KawaiiTextureFilter::Linear;

      case KawaiiTextureFilter::NearestMipmapLinear:
      case KawaiiTextureFilter::NearestMipmapNearest:
        return KawaiiTextureFilter::Nearest;

      default:
        return filter;
      }
  }
}

//todo: explicit mipmap

SarielTextureHandle::SarielTextureHandle(SarielRootImpl &root_, KawaiiTexture *model):
  r(root_),
  tex(trTextureType(model->getType())),
  storageType(StorageType::None),
  mipmap_needed(checkMipmapIsNeeded(model->getMinFilter()))
{
  r.taskGL([this, model] {
      tex.create();
      tex.setMinMagFilters(trTextureFilter(model->getMinFilter()),
                           trTextureFilter(filterNoMipmap(model->getMagFilter())));

      tex.setWrapMode(QOpenGLTexture::DirectionS, trWrapMode(model->getWrapModeS()));
      if(model->getType() != KawaiiTextureType::Texture1D
         && model->getType() != KawaiiTextureType::Texture1D_Array
         && model->getType() != KawaiiTextureType::TextureBuffer)
        {
          tex.setWrapMode(QOpenGLTexture::DirectionT, trWrapMode(model->getWrapModeT()));
          if(model->getType() == KawaiiTextureType::Texture3D)
            {
              tex.setWrapMode(QOpenGLTexture::DirectionR, trWrapMode(model->getWrapModeR()));
            }
        }
    });
}

SarielTextureHandle::~SarielTextureHandle()
{
  r.taskGL([this] {
      tex.destroy();
    });
}

void SarielTextureHandle::attachToFbo(GLuint fboId, GLenum attachment, GLint layer, GLuint level)
{
  if(storageType == StorageType::None)
    {
      qWarning("SarielRenderer warning: attempt to attach an empty texture to framebuffer");
      return;
    }

  r.gl().glBindFramebuffer(GL_FRAMEBUFFER, fboId);
  if(layer < 0)
    r.gl().glFramebufferTexture(GL_FRAMEBUFFER, attachment, tex.textureId(), level);
  else
    r.gl().glFramebufferTextureLayer(GL_FRAMEBUFFER, attachment, tex.textureId(), level, layer);
}

QOpenGLTexture &SarielTextureHandle::getTexture()
{
  return tex;
}

void SarielTextureHandle::setData1D(int width, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr)
{
  if(!checkStorageCompatibility(width, fmt)
     || (tex.isStorageAllocated() && tex.format() != trInternalTexFormat<decltype(*ptr)>(fmt)))
    return invalidate();

  r.asyncTaskGL([this, width, layers, fmt, ptr] {
      if(!tex.isStorageAllocated())
        {
          tex.setSize(width);
          if(layers > 0)
            tex.setLayers(layers);
          tex.setMipLevels(mipmap_needed? 4: 1);
          tex.setFormat(trInternalTexFormat<decltype(*ptr)>(fmt));
          tex.allocateStorage();
        }
      if(layers < 0)
        {
          if(ptr)
            tex.setData(trTexFormat(fmt), trArgType(ptr), ptr);
          storageType = StorageType::Data1D;
        } else
        {
          if(ptr)
            tex.setData(0, 0, layers, QOpenGLTexture::CubeMapPositiveX, trTexFormat(fmt), trArgType(ptr), ptr);
          storageType = StorageType::Data2D;
        }
      storageFmt = fmt;
    });
}

void SarielTextureHandle::setData1D(int width, int layers, KawaiiTextureFormat fmt, const float *ptr)
{
  if(!checkStorageCompatibility(width, fmt)
     || (tex.isStorageAllocated() && tex.format() != trInternalTexFormat<decltype(*ptr)>(fmt)))
    return invalidate();

  r.asyncTaskGL([this, width, layers, fmt, ptr] {
      if(!tex.isStorageAllocated())
        {
          tex.setSize(width);
          if(layers > 0)
            tex.setLayers(layers);
          tex.setMipLevels(mipmap_needed? 4: 1);
          tex.setFormat(trInternalTexFormat<decltype(*ptr)>(fmt));
          tex.allocateStorage();
        }
      if(layers < 0)
        {
          if(ptr)
            tex.setData(trTexFormat(fmt), trArgType(ptr), ptr);
          storageType = StorageType::Data1D;
        } else
        {
          if(ptr)
            tex.setData(0, 0, layers, QOpenGLTexture::CubeMapPositiveX, trTexFormat(fmt), trArgType(ptr), ptr);
          storageType = StorageType::Data2D;
        }
      storageFmt = fmt;
    });
}

void SarielTextureHandle::setData2D(int width, int heigh, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr)
{
  if(!checkStorageCompatibility(width, heigh, fmt)
     || (tex.isStorageAllocated() && tex.format() != trInternalTexFormat<decltype(*ptr)>(fmt)))
    return invalidate();

  r.asyncTaskGL([this, width, heigh, layers, fmt, ptr] {
      if(!tex.isStorageAllocated())
        {
          tex.setSize(width, heigh);
          if(layers > 0)
            tex.setLayers(layers);
          tex.setMipLevels(mipmap_needed? 4: 1);
          tex.setFormat(trInternalTexFormat<decltype(*ptr)>(fmt));
          tex.allocateStorage();
        }
      if(layers < 0)
        {
          if(ptr)
            tex.setData(trTexFormat(fmt), trArgType(ptr), ptr);
          storageType = StorageType::Data2D;
        } else
        {
          if(ptr)
            tex.setData(0, 0, layers, QOpenGLTexture::CubeMapPositiveX, trTexFormat(fmt), trArgType(ptr), ptr);
          storageType = StorageType::Data3D;
        }
      storageFmt = fmt;
    });
}

void SarielTextureHandle::setData2D(int width, int heigh, int layers, KawaiiTextureFormat fmt, const float *ptr)
{
  if(!checkStorageCompatibility(width, heigh, fmt)
     || (tex.isStorageAllocated() && tex.format() != trInternalTexFormat<decltype(*ptr)>(fmt)))
    return invalidate();

  r.asyncTaskGL([this, width, heigh, layers, fmt, ptr] {
      if(!tex.isStorageAllocated())
        {
          tex.setSize(width, heigh);
          if(layers > 0)
            tex.setLayers(layers);
          tex.setMipLevels(mipmap_needed? 4: 1);
          tex.setFormat(trInternalTexFormat<decltype(*ptr)>(fmt));
          tex.allocateStorage();
        }
      if(layers < 0)
        {
          if(ptr)
            tex.setData(trTexFormat(fmt), trArgType(ptr), ptr);
          storageType = StorageType::Data2D;
        } else
        {
          if(ptr)
            tex.setData(0, 0, layers, QOpenGLTexture::CubeMapPositiveX, trTexFormat(fmt), trArgType(ptr), ptr);
          storageType = StorageType::Data3D;
        }
      storageFmt = fmt;
    });
}

void SarielTextureHandle::setData3D(int width, int heigh, int depth, KawaiiTextureFormat fmt, const uint8_t *ptr)
{
  if(!checkStorageCompatibility(width, heigh, depth, fmt)
     || (tex.isStorageAllocated() && tex.format() != trInternalTexFormat<decltype(*ptr)>(fmt)))
    return invalidate();

  r.asyncTaskGL([this, width, heigh, depth, fmt, ptr] {
      if(!tex.isStorageAllocated())
        {
          tex.setSize(width, heigh, depth);
          tex.setMipLevels(mipmap_needed? 4: 1);
          tex.setFormat(trInternalTexFormat<decltype(*ptr)>(fmt));
          tex.allocateStorage();
        }
      if(ptr)
        tex.setData(trTexFormat(fmt), trArgType(ptr), ptr);
      storageType = StorageType::Data3D;
      storageFmt = fmt;
    });
}

void SarielTextureHandle::setData3D(int width, int heigh, int depth, KawaiiTextureFormat fmt, const float *ptr)
{
  if(!checkStorageCompatibility(width, heigh, depth, fmt)
     || (tex.isStorageAllocated() && tex.format() != trInternalTexFormat<decltype(*ptr)>(fmt)))
    return invalidate();

  r.asyncTaskGL([this, width, heigh, depth, fmt, ptr] {
      if(!tex.isStorageAllocated())
        {
          tex.setSize(width, heigh, depth);
          tex.setMipLevels(mipmap_needed? 4: 1);
          tex.setFormat(trInternalTexFormat<decltype(*ptr)>(fmt));
          tex.allocateStorage();
        }
      if(ptr)
        tex.setData(trTexFormat(fmt), trArgType(ptr), ptr);
      storageType = StorageType::Data3D;
      storageFmt = fmt;
    });
}

void SarielTextureHandle::setDataCube(int width, int heigh, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr)
{
  if(!checkStorageCompatibility(width, heigh, 6, fmt)
     || (tex.isStorageAllocated() && tex.format() != trInternalTexFormat<decltype(*ptr)>(fmt)))
    return invalidate();

  r.asyncTaskGL([this, width, heigh, layers, fmt, ptr] {
      if(!tex.isStorageAllocated())
        {
          tex.setSize(width, heigh);
          if(layers > 0)
            tex.setLayers(layers);
          tex.setMipLevels(mipmap_needed? 4: 1);
          tex.setFormat(trInternalTexFormat<decltype(*ptr)>(fmt));
          tex.allocateStorage();
        }
      if(ptr)
        {
          const GLenum glFmt = trTexGlFormat(fmt);
          tex.bind();
          if(layers > 0)
            r.gl().glTexSubImage3D(GL_TEXTURE_CUBE_MAP_ARRAY, 0, 0, 0, 0, width, heigh, 6*layers, glFmt, GL_UNSIGNED_BYTE, ptr);
          else
            {
              for(size_t i = 0; i < 6; ++i)
                r.gl().glTexSubImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, 0, 0, width, heigh, glFmt, GL_UNSIGNED_BYTE, ptr + 4*width*heigh*i);
            }
          tex.release();
        }

      storageType = StorageType::Data3D;
      storageFmt = fmt;
    });
}

void SarielTextureHandle::setDataCube(int width, int heigh, int layers, KawaiiTextureFormat fmt, const float *ptr)
{
  if(!checkStorageCompatibility(width, heigh, 6, fmt)
     || (tex.isStorageAllocated() && tex.format() != trInternalTexFormat<decltype(*ptr)>(fmt)))
    return invalidate();

  r.asyncTaskGL([this, width, heigh, layers, fmt, ptr] {
      if(!tex.isStorageAllocated())
        {
          tex.setSize(width, heigh);
          if(layers > 0)
            tex.setLayers(layers);
          tex.setMipLevels(mipmap_needed? 4: 1);
          tex.setFormat(trInternalTexFormat<decltype(*ptr)>(fmt));
          tex.allocateStorage();
        }
      if(ptr)
        {
          const GLenum glFmt = trTexGlFormat(fmt);
          tex.bind();
          if(layers > 0)
            r.gl().glTexSubImage3D(GL_TEXTURE_CUBE_MAP_ARRAY, 0, 0, 0, 0, width, heigh, 6*layers, glFmt, GL_FLOAT, ptr);
          else
            {
              for(size_t i = 0; i < 6; ++i)
                r.gl().glTexSubImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, 0, 0, width, heigh, glFmt, GL_FLOAT, ptr + 4*width*heigh*i);
            }
          tex.release();
        }
      storageType = StorageType::Data3D;
      storageFmt = fmt;
    });
}

void SarielTextureHandle::setCompareOperation(KawaiiDepthCompareOperation op)
{
  switch(op)
    {
    case KawaiiDepthCompareOperation::None:
      r.asyncTaskGL([this] {
          tex.setComparisonMode(QOpenGLTexture::CompareNone);
        });
      break;
    case KawaiiDepthCompareOperation::Less:
      r.asyncTaskGL([this] {
          tex.setComparisonMode(QOpenGLTexture::CompareRefToTexture);
          tex.setComparisonFunction(QOpenGLTexture::CompareLess);
        });
      break;
    case KawaiiDepthCompareOperation::LessEqual:
      r.asyncTaskGL([this] {
          tex.setComparisonMode(QOpenGLTexture::CompareRefToTexture);
          tex.setComparisonFunction(QOpenGLTexture::CompareLessEqual);
        });
      break;
    case KawaiiDepthCompareOperation::Greater:
      r.asyncTaskGL([this] {
          tex.setComparisonMode(QOpenGLTexture::CompareRefToTexture);
          tex.setComparisonFunction(QOpenGLTexture::CompareGreater);
        });
      break;
    case KawaiiDepthCompareOperation::GreaterEqual:
      r.asyncTaskGL([this] {
          tex.setComparisonMode(QOpenGLTexture::CompareRefToTexture);
          tex.setComparisonFunction(QOpenGLTexture::CompareGreaterEqual);
        });
      break;
    }
}

void SarielTextureHandle::setMinFilter(KawaiiTextureFilter filter)
{
  bool mipmap = checkMipmapIsNeeded(filter);
  if(mipmap != mipmap_needed)
    {
      if(storageType == StorageType::None)
        mipmap_needed = mipmap;
      else
        return invalidate();
    }

  r.asyncTaskGL([this, filter] {
      tex.setMinificationFilter(trTextureFilter(filter));
    });
}

void SarielTextureHandle::setMagFilter(KawaiiTextureFilter filter)
{
  bool mipmap = checkMipmapIsNeeded(filter);
  if(mipmap != mipmap_needed)
    {
      if(storageType == StorageType::None)
        mipmap_needed = mipmap;
      else
        return invalidate();
    }

  r.asyncTaskGL([this, filter] {
      tex.setMagnificationFilter(trTextureFilter(filter));
    });
}

void SarielTextureHandle::setWrapModeS(KawaiiTextureWrapMode mode)
{
  r.asyncTaskGL([this, mode] {
      tex.setWrapMode(QOpenGLTexture::DirectionS, trWrapMode(mode));
    });
}

void SarielTextureHandle::setWrapModeT(KawaiiTextureWrapMode mode)
{
  if(tex.target() != QOpenGLTexture::Target1D
     && tex.target() != QOpenGLTexture::Target1DArray
     && tex.target() != QOpenGLTexture::TargetBuffer)
    {
      r.asyncTaskGL([this, mode] {
          tex.setWrapMode(QOpenGLTexture::DirectionT, trWrapMode(mode));
        });
    }
}

void SarielTextureHandle::setWrapModeR(KawaiiTextureWrapMode mode)
{
  if(tex.target() == QOpenGLTexture::Target3D)
    {
      r.asyncTaskGL([this, mode] {
          tex.setWrapMode(QOpenGLTexture::DirectionR, trWrapMode(mode));
        });
    }
}

bool SarielTextureHandle::checkMipmapIsNeeded(KawaiiTextureFilter filter) const
{
  switch(filter)
    {
    case KawaiiTextureFilter::LinearMipmapLinear:
    case KawaiiTextureFilter::LinearMipmapNearest:
    case KawaiiTextureFilter::NearestMipmapLinear:
    case KawaiiTextureFilter::NearestMipmapNearest:
      return true;
      break;

    default:
      return false;
      break;
    }
}

bool SarielTextureHandle::checkStorageCompatibility(int width, KawaiiTextureFormat fmt) const
{
  bool storageTypeMatches = (storageType == StorageType::Data1D);
  bool sizeMatches = (width == tex.width());
  bool fmtMatches = (fmt == storageFmt);
  return (storageType == StorageType::None || (storageTypeMatches && sizeMatches && fmtMatches));
}

bool SarielTextureHandle::checkStorageCompatibility(int width, int height, KawaiiTextureFormat fmt) const
{
  bool storageTypeMatches = (storageType == StorageType::Data2D);
  bool sizeMatches = (width == tex.width() && height == tex.height());
  bool fmtMatches = (fmt == storageFmt);
  return (storageType == StorageType::None || (storageTypeMatches && sizeMatches && fmtMatches));
}

bool SarielTextureHandle::checkStorageCompatibility(int width, int height, int depth, KawaiiTextureFormat fmt) const
{
  bool storageTypeMatches = (storageType == StorageType::Data3D);
  bool sizeMatches = (width == tex.width() && height == tex.height() && depth == tex.depth());
  bool fmtMatches = (fmt == storageFmt);
  return (storageType == StorageType::None || (storageTypeMatches && sizeMatches && fmtMatches));
}
