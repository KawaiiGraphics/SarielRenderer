#include "KawaiiSarielFactory.hpp"
#include "SarielRootImpl.hpp"
#include <QWindow>

extern "C" SARIELRENDERER_SHARED_EXPORT KawaiiRendererImpl* createSarielRoot(KawaiiRoot *root)
{
  return KawaiiSarielFactory::getInstance()->createRenderer(*root);
}

extern "C" SARIELRENDERER_SHARED_EXPORT bool checkSariel()
{
  return SarielRootImpl::checkSystem();
}
