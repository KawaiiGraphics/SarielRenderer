#include "SarielGpuBufImpl.hpp"
#include "SarielRootImpl.hpp"
#include <KawaiiRenderer/Textures/KawaiiTextureImpl.hpp>
#include <Kawaii3D/KawaiiConfig.hpp>
#include <cstring>

SarielGpuBufImpl::SarielGpuBufImpl(KawaiiGpuBuf *model):
  KawaiiGpuBufImpl(model),
  oldRoot(nullptr)
{
  static_cast<SarielBufferHandle*>(getHandle())->setUboPostBindFunc(std::bind(&SarielGpuBufImpl::onBind, this, std::placeholders::_1, std::placeholders::_2));
  static_cast<SarielBufferHandle*>(getHandle())->setUboPostUnbindFunc(std::bind(&SarielGpuBufImpl::onUnbind, this, std::placeholders::_1, std::placeholders::_2));
  onRootChanged = std::bind(&SarielGpuBufImpl::rootChanged, this);
  rootChanged();
}

SarielGpuBufImpl::~SarielGpuBufImpl()
{
}

void SarielGpuBufImpl::bindTexture(const QString &fieldName, KawaiiTextureImpl *tex)
{
  const auto str = fieldName.toStdString();
  boundTextures[str] = tex;
  auto *root = static_cast<SarielRootImpl*>(getRoot());
  if(root && root->isUboBinded(this))
    root->bindTexture(str, static_cast<SarielTextureHandle*>(tex->getHandle()));
}

void SarielGpuBufImpl::unbindTexture(const QString &fieldName, KawaiiTextureImpl *tex)
{
  std::string std_fieldName = fieldName.toStdString();

  auto el = boundTextures.find(std_fieldName);
  if(el != boundTextures.end() && el->second == tex)
    {
      boundTextures.erase(el);
      auto *root = static_cast<SarielRootImpl*>(getRoot());
      if(root && root->isUboBinded(this))
        root->releaseTexture(static_cast<SarielTextureHandle*>(tex->getHandle()));
    }
}

void SarielGpuBufImpl::rootChanged()
{
  if(oldRoot)
    {
      for(const auto &i: boundTextures)
        oldRoot->releaseTexture(static_cast<SarielTextureHandle*>(i.second->getHandle()));
    }
  oldRoot = static_cast<SarielRootImpl*>(getRoot());
  rebindTextures();
}

void SarielGpuBufImpl::rebindTextures()
{
  auto *root = static_cast<SarielRootImpl*>(getRoot());
  if(root)
    for(const auto &i: boundTextures)
      root->bindTexture(i.first, static_cast<SarielTextureHandle*>(i.second->getHandle()));
}

void SarielGpuBufImpl::unbindTextures()
{
  auto *root = static_cast<SarielRootImpl*>(getRoot());
  if(root)
    for(const auto &i: boundTextures)
      root->releaseTexture(static_cast<SarielTextureHandle*>(i.second->getHandle()));
}

void SarielGpuBufImpl::onBind(GLenum mode, uint32_t index)
{
  rebindTextures();
  auto *root = static_cast<SarielRootImpl*>(getRoot());
  if(root)
    root->setBindedUbo(mode, index, this);
}

void SarielGpuBufImpl::onUnbind(GLenum mode, uint32_t index)
{
  unbindTextures();
  auto *root = static_cast<SarielRootImpl*>(getRoot());
  if(root)
    root->setBindedUbo(mode, index, 0);
}
