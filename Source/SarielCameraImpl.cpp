#include "SarielCameraImpl.hpp"
#include "SarielRootImpl.hpp"
#include <qopengl.h>

SarielCameraImpl::SarielCameraImpl(KawaiiCamera *model):
  KawaiiCameraImpl(model)
{
}

KawaiiCameraImpl::DrawCache *SarielCameraImpl::createDrawCache(KawaiiGpuBufImpl *sfcUbo, const QRect &viewport)
{
  static_cast<SarielRootImpl*>(getModel()->getScene()->getRoot()->getRendererImpl())->gl().glViewport(viewport.x(), viewport.y(), viewport.width(), viewport.height());
  drawScene(sfcUbo);
  return nullptr;
}
