#ifndef KAWAIISARIELFACTORY_HPP
#define KAWAIISARIELFACTORY_HPP

#include <Kawaii3D/KawaiiImplFactory.hpp>
#include "SarielRenderer_global.hpp"

class SARIELRENDERER_SHARED_EXPORT KawaiiSarielFactory : public KawaiiImplFactory
{
  KawaiiSarielFactory();
  ~KawaiiSarielFactory() = default;

public:
  static KawaiiImplFactory* getInstance();
  static void deleteInstance();



  //IMPLEMENT
private:
  static KawaiiSarielFactory *instance;
  static void createInstance();
};

#endif // KAWAIISARIELFACTORY_HPP
