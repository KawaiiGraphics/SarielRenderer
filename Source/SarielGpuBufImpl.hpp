#ifndef SARIELGPUBUFIMPL_HPP
#define SARIELGPUBUFIMPL_HPP

#include <KawaiiRenderer/KawaiiGpuBufImpl.hpp>
#include "SarielBufferHandle.hpp"
#include <unordered_map>
#include <unordered_set>
#include <qopengl.h>
#include <QPointer>

class SARIELRENDERER_SHARED_EXPORT SarielGpuBufImpl : public KawaiiGpuBufImpl
{
public:
  SarielGpuBufImpl(KawaiiGpuBuf *model);
  ~SarielGpuBufImpl();

  // KawaiiGpuBufImpl interface
  void bindTexture(const QString &fieldName, KawaiiTextureImpl *tex) override final;
  void unbindTexture(const QString &fieldName, KawaiiTextureImpl *tex) override final;



  //IMPLEMENT
private:
  SarielRootImpl *oldRoot;
  std::unordered_map<std::string, QPointer<KawaiiTextureImpl>> boundTextures;

  void rootChanged();

  void rebindTextures();
  void unbindTextures();

  void onBind(GLenum mode, uint32_t index);
  void onUnbind(GLenum mode, uint32_t index);
};

#endif // SARIELGPUBUFIMPL_HPP
