#include "glsl_merger.hpp"
#include <QVector>

glsl_merger::glsl_merger(QString pattern, bool defined_value):
  str(pattern.replace(' ', "\\s+")),

  plain((!defined_value?
          str.arg("(?<type>\\w+)", "(?<name>\\w+)"):
          str.arg("(?<type>\\w+)", "(?<name>\\w+)", "\\s*=\\s*(?<value>.+)"))+"\\s*;"),

  expl_layout("layout\\s*\\(\\s*location\\s*=\\s*\\d+\\s*\\)"+plain.pattern()),
  defined_value(defined_value)
{
  plain.optimize();
  expl_layout.optimize();
}

QVector<bool> get_global_scope(const QString &src)
{
  QVector<bool> is_global(src.size(), false);
  int level = 0,
      level_ = 0,
      prepr_d = 0;
  for(int k = 0; k < src.size(); ++k)
    switch (src.at(k).toLatin1())
      {
      case '{':
        ++level;
        break;
      case '}':
        --level;
        break;
      case '(':
        ++level_;
        break;
      case ')':
        --level_;
        break;
      case '#':
        prepr_d = 1;
        break;
      case '\n':
        prepr_d = 0;
        [[fallthrough]];
      default:
        if(!level && !level_ && !prepr_d)
          is_global[k] = true;
        break;
      }
  return is_global;
}

void remove(QString &str, QRegularExpression &exp, QVector<bool> &global)
{
  auto i = exp.globalMatch(str);
  while(i.hasNext())
    {
      auto token = i.next();
      int i0 = token.capturedStart(0);

      if(global.at(i0))
        {
          str.remove(i0, token.capturedLength(0));
          global = get_global_scope(str);
          i = exp.globalMatch(str);
        }
    }
}

void glsl_merger::garant_unique(QString &a, QString &b, QRegularExpression &exp, QVector<bool> &gs_a, QVector<bool> &gs_b)
{
  auto i = exp.globalMatch(a);
  while(i.hasNext())
    {
      auto token = i.next();
      if(!gs_a[token.capturedStart(0)]) continue;

      QRegularExpression current_exp((!defined_value?
                                       str.arg(token.captured("type"), token.captured("name")):
                                       str.arg(token.captured("type"), token.captured("name"), "\\s*=\\s*"+token.captured("value")))
                                     +"\\s*;");

      remove(b, current_exp, gs_b);

      if(defined_value)
        {
          current_exp.setPattern(str.arg(token.captured("type"), token.captured("name"), "\\s*;"));
          remove(b, current_exp, gs_b);
        }
    }
}

void glsl_merger::merge(QString &old, QString &append, QVector<bool> &gs_a, QVector<bool> &gs_b)
{
  if(gs_a.size() != old.size())
    {
      gs_a = get_global_scope(old);
      gs_b = get_global_scope(append);
    }
  garant_unique(old, append, plain, gs_a, gs_b);
  garant_unique(old, append, expl_layout, gs_a, gs_b);
  if(defined_value)
    garant_unique(append, old, plain, gs_b, gs_a);

  garant_unique(append, old, expl_layout, gs_b, gs_a);
}
