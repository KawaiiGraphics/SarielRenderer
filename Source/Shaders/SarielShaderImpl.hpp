#ifndef SARIELSHADERIMPL_HPP
#define SARIELSHADERIMPL_HPP

#include <KawaiiRenderer/Shaders/KawaiiShaderImpl.hpp>
#include "../SarielRenderer_global.hpp"
#include <QOpenGLShader>
#include <functional>
#include <memory>

class SarielRootImpl;
class SARIELRENDERER_SHARED_EXPORT SarielShaderImpl : public KawaiiShaderImpl
{
public:
  SarielShaderImpl(KawaiiShader *model);
  ~SarielShaderImpl();

  void useInProgram(QOpenGLShaderProgram &prog);

  const QString &getCode() const;

  QOpenGLShader::ShaderTypeBit getType() const;

  void forallUboLocations(const std::function<void(const QString&, GLuint)> &func) const;

  // KawaiiShaderImpl interface
private:
  void createShader() override final;
  void deleteShader() override final;
  void recompile() override final;



  //IMPLEMENT
private:
  KawaiiShader *modelObj;
  std::unique_ptr<QOpenGLShader> shader;
  QString code;
  QHash<QString, GLuint> requiredUboLocations;

  QString preprocessGlsl(const QString &code);
  SarielRootImpl* rootImpl();

  void createShaderWork();
  void deleteShaderWork();
  void recompileWork();

  void printLog();
};

#endif // SARIELSHADERIMPL_HPP
