#ifndef GLSL_MERGER_HPP
#define GLSL_MERGER_HPP

#include <QRegularExpression>
#include <QString>

class glsl_merger
{
  QString str;
  QRegularExpression plain;
  QRegularExpression expl_layout;
  bool defined_value;
  void garant_unique(QString &a, QString &b, QRegularExpression &exp, QVector<bool> &gs_a, QVector<bool> &gs_b);

public:
  glsl_merger(QString pattern, bool defined_value=false);
  virtual ~glsl_merger() = default;

  void merge(QString &old, QString &append, QVector<bool> &gs_a, QVector<bool> &gs_b);
};

#endif // GLSL_MERGER_HPP
