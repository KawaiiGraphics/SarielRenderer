#ifndef SARIELPROGRAMIMPL_HPP
#define SARIELPROGRAMIMPL_HPP

#include <KawaiiRenderer/Shaders/KawaiiProgramImpl.hpp>
#include "../SarielRenderer_global.hpp"
#include <QOpenGLShaderProgram>
#include <memory>

class SarielRootImpl;
class SarielShaderImpl;
class SARIELRENDERER_SHARED_EXPORT SarielProgramImpl : public KawaiiProgramImpl
{
public:
  SarielProgramImpl(KawaiiProgram *model);
  ~SarielProgramImpl();

  bool getUseMegashaders() const;

  // KawaiiProgramImpl interface
public:
  bool use() const override final;

private:
  void createProgram() override final;
  void deleteProgram() override final;
  void reAddShader(KawaiiShaderImpl *shader) override final;



  //IMPLEMENT
private:
  std::unique_ptr<QOpenGLShaderProgram> prog;
  std::unordered_map<QOpenGLShader::ShaderTypeBit, QString> megashadersCode;
  bool useMegashaders;

  SarielRootImpl* rootImpl() const;

  void createProgramWork();
  void deleteProgramWork();
  void reAddShaderWork(KawaiiShaderImpl *shader);

  void printLog();
};

#endif // SARIELPROGRAMIMPL_HPP
