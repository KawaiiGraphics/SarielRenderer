#include "SarielProgramImpl.hpp"
#include "SarielShaderImpl.hpp"
#include "SarielRootImpl.hpp"
#include "glsl_merger.hpp"
#include <Kawaii3D/KawaiiConfig.hpp>
#include <QSet>

SarielProgramImpl::SarielProgramImpl(KawaiiProgram *model):
  KawaiiProgramImpl(model),
  prog(std::make_unique<QOpenGLShaderProgram>(this)),
  useMegashaders(rootImpl()->isOpenGLES())
{
}

SarielProgramImpl::~SarielProgramImpl()
{
  rootImpl()->resetUniformLocations(prog.get());
  preDestruct();
}

bool SarielProgramImpl::getUseMegashaders() const
{
  return useMegashaders;
}

bool SarielProgramImpl::use() const
{
  if(!prog->isLinked())
    {
      const_cast<SarielProgramImpl*>(this)->createProgramWork();
    }

  return rootImpl()->useShaderProg(*prog);
}

void SarielProgramImpl::createProgram()
{
  rootImpl()->taskGL(std::bind(&SarielProgramImpl::createProgramWork, this));
}

void SarielProgramImpl::deleteProgram()
{
  rootImpl()->taskGL(std::bind(&SarielProgramImpl::deleteProgramWork, this));
}

void SarielProgramImpl::reAddShader(KawaiiShaderImpl *shader)
{
  rootImpl()->asyncTaskGL(std::bind(&SarielProgramImpl::reAddShaderWork, this, shader));
}

SarielRootImpl *SarielProgramImpl::rootImpl() const
{
  return static_cast<SarielRootImpl*>(getRoot());
}

namespace {
  QRegularExpression versionExp("#version.*\n");

//  QString getObjStr(const QObject *obj)
//  {
//    if(obj->objectName().isEmpty() || obj->objectName().isNull())
//      return QLatin1String(obj->metaObject()->className()) + QStringLiteral("_%1").arg(reinterpret_cast<qlonglong>(obj));
//    else
//      return obj->objectName();
//  }
}

void SarielProgramImpl::createProgramWork()
{
  rootImpl()->resetUniformLocations(prog.get());
  if(useMegashaders)
    {
      megashadersCode.clear();
      forallShaders([this] (KawaiiShaderImpl *shader) {
        auto sh = static_cast<SarielShaderImpl*>(shader);
        if(auto el = megashadersCode.find(sh->getType()); el != megashadersCode.end())
          {
            static glsl_merger mergers[] = {glsl_merger("uniform %1 %2"),
                                            glsl_merger("in %1 %2"),
                                            glsl_merger("precision %1 %2"),
                                            glsl_merger("%1 %2"),
                                            glsl_merger("%1 %2%3", true)
                                           };
            QString addingCode = sh->getCode();
            QVector<bool> el_global, addingCode_global;
            addingCode.remove(versionExp);
            for(auto &m: mergers)
              m.merge(el->second, addingCode, el_global, addingCode_global);
            el->second += "\n\n//NEW SHADER\n\n" + addingCode;
          } else
          megashadersCode.insert({sh->getType(), sh->getCode()});
      });
      for(auto code: megashadersCode)
        {
          QSet<QString> extensions;
          static QRegularExpression extensionExp("#extension.*\n");
          auto i = extensionExp.globalMatch(code.second);
          while(i.hasNext())
            extensions.insert(i.next().captured());
          code.second.remove(extensionExp);
          int afterVersionI = versionExp.match(code.second).capturedEnd();
          for(const auto &ext: extensions)
            code.second.insert(afterVersionI, ext);
          prog->addCacheableShaderFromSourceCode(code.first, code.second);
        }
    } else
    {
      forallShaders([this] (KawaiiShaderImpl *shader) {
        static_cast<SarielShaderImpl*>(shader)->useInProgram(*prog);
      });
    }

  prog->link();
  if(prog->isLinked() && !rootImpl()->shadersHas420Pack())
    {
      const GLuint progId = prog->programId();
      auto &gl = rootImpl()->gl();

      forallShaders([&gl, progId] (KawaiiShaderImpl *shader) {
          static_cast<SarielShaderImpl*>(shader)->forallUboLocations([&gl, progId] (const QString &name, GLuint binding) {
              const GLuint blockIndex = gl.glGetUniformBlockIndex(progId, reinterpret_cast<const GLchar*>(name.toUtf8().constData()));
              if(blockIndex != GL_INVALID_INDEX)
                gl.glUniformBlockBinding(progId, blockIndex, binding);
            });
      });
    }

//  QDir d(QDir::temp());
//  const auto dumpPath = QLatin1String("SarielShaders/") + getObjStr(this);
//  d.mkpath(dumpPath);
//  d.cd(dumpPath);
//  forallShaders([&d] (KawaiiShaderImpl *shader) {
//    auto sh = static_cast<SarielShaderImpl*>(shader);
//    const auto code = sh->getCode();
//    QFile f(d.absoluteFilePath(getObjStr(shader)+QStringLiteral(".%1.glsl").arg(sh->getType())));
//    f.open(QFile::WriteOnly | QFile::Text);
//    QTextStream st(&f);
//    st << code;
//    f.close();
//  });

  printLog();
}

void SarielProgramImpl::deleteProgramWork()
{
  rootImpl()->resetUniformLocations(prog.get());
  prog = std::make_unique<QOpenGLShaderProgram>(this);
}

void SarielProgramImpl::reAddShaderWork([[maybe_unused]] KawaiiShaderImpl *shader)
{
  //  static_cast<SarielShaderImpl*>(shader)->useInProgram(id);
  //  rootImpl()->gl().glLinkProgram();
  if(useMegashaders)
    return createProgramWork();

  rootImpl()->resetUniformLocations(prog.get());
  prog->link();
  printLog();
}

void SarielProgramImpl::printLog()
{
  if(!prog->isLinked() && KawaiiConfig::getInstance().getDebugLevel() > 0)
    {
      qCritical().noquote() << objectName()
                            << " (SarielProgram): Linking failed!\n" << prog->log();
      qCritical().quote();
    }
}
