#include "SarielShaderImpl.hpp"
#include <SarielRootImpl.hpp>
#include <sib_utils/Strings.hpp>
#include <Kawaii3D/KawaiiConfig.hpp>
#include <QRegularExpression>

namespace {
  QOpenGLShader::ShaderTypeBit trShaderType(KawaiiShaderType type)
  {
    switch(type)
      {
      case KawaiiShaderType::Vertex:
        return QOpenGLShader::ShaderTypeBit::Vertex;
      case KawaiiShaderType::Fragment:
        return QOpenGLShader::ShaderTypeBit::Fragment;
      case KawaiiShaderType::Compute:
        return QOpenGLShader::ShaderTypeBit::Compute;
      case KawaiiShaderType::Geometry:
        return QOpenGLShader::ShaderTypeBit::Geometry;
      case KawaiiShaderType::TesselationControll:
        return QOpenGLShader::ShaderTypeBit::TessellationControl;
      case KawaiiShaderType::TesselationEvaluion:
        return QOpenGLShader::ShaderTypeBit::TessellationEvaluation;
      }
    Q_UNREACHABLE();
  }
}

SarielShaderImpl::SarielShaderImpl(KawaiiShader *model):
  KawaiiShaderImpl(model),
  modelObj(model),
  shader(nullptr)
{
  initResource();
}

SarielShaderImpl::~SarielShaderImpl()
{
  preDestruct();
}

void SarielShaderImpl::useInProgram(QOpenGLShaderProgram &prog)
{
  if(!shader)
    createShaderWork();

  prog.addShader(shader.get());
}

const QString &SarielShaderImpl::getCode() const
{
  return code;
}

QOpenGLShader::ShaderTypeBit SarielShaderImpl::getType() const
{
  if(shader->shaderType().testFlag(QOpenGLShader::Vertex))
    return QOpenGLShader::Vertex;
  else if(shader->shaderType().testFlag(QOpenGLShader::Fragment))
    return QOpenGLShader::Fragment;
  else if(shader->shaderType().testFlag(QOpenGLShader::Geometry))
    return QOpenGLShader::Geometry;
  else if(shader->shaderType().testFlag(QOpenGLShader::Compute))
    return QOpenGLShader::Compute;
  else if(shader->shaderType().testFlag(QOpenGLShader::TessellationControl))
    return QOpenGLShader::TessellationControl;
  else if(shader->shaderType().testFlag(QOpenGLShader::TessellationEvaluation))
    return QOpenGLShader::TessellationEvaluation;
  Q_UNREACHABLE();
}

void SarielShaderImpl::forallUboLocations(const std::function<void (const QString &, GLuint)> &func) const
{
  if(func)
    for(auto i = requiredUboLocations.cbegin(); i != requiredUboLocations.cend(); ++i)
      func(i.key(), i.value());
}

void SarielShaderImpl::createShader()
{
  rootImpl()->taskGL(std::bind(&SarielShaderImpl::createShaderWork, this));
}

void SarielShaderImpl::deleteShader()
{
  rootImpl()->taskGL(std::bind(&SarielShaderImpl::deleteShaderWork, this));
}

void SarielShaderImpl::recompile()
{
  if(shader)
    rootImpl()->asyncTaskGL(std::bind(&SarielShaderImpl::recompileWork, this));
}

namespace {
  QRegularExpression preparePattern(QString str)
  {
    while(str.contains(QLatin1String("  ")))
      str.replace(QLatin1String("  "), QLatin1String(" "));
    str.replace(QLatin1String(" "), QLatin1String("\\s*"));
    return QRegularExpression(str);
  }
}

QString SarielShaderImpl::preprocessGlsl(const QString &code)
{
  QString glsl = code;

  QMap<int, QString> injections;

  auto injectTextureHandles = [&injections](int blockEnd, const QHash<QString, QString> &textures) {
      QString global;
      const QString globTerm = QStringLiteral("uniform %2 %1\n");
      for(auto i = textures.begin(); i != textures.end(); ++i)
          global += QStringLiteral("uniform %1;\n").arg(*i);
      injections.insert(blockEnd+1, global);
    };

  injectTextureHandles(modelObj->getCameraBlockEnd(),   modelObj->getCameraTextures());
  injectTextureHandles(modelObj->getSurfaceBlockEnd(),  modelObj->getSurfaceTextures());
  injectTextureHandles(modelObj->getMaterialBlockEnd(), modelObj->getMaterialTextures());
  injectTextureHandles(modelObj->getModelBlockEnd(),    modelObj->getModelTextures());
  injectTextureHandles(-1, modelObj->getGlobalTextures());

  static QRegularExpression regEx(QStringLiteral("#version\\s+\\d+\\s+core\\s*"));
  const auto versionMatch = regEx.match(glsl);
  int offset = 0;
  if(versionMatch.hasMatch())
    offset = versionMatch.capturedEnd();
  else
    {
      const auto versionStr = rootImpl()->getGlslVersion();
      glsl = versionStr + glsl;
      offset = versionStr.size();
    }

  for(auto i = injections.end()-1; i != injections.begin()-1; --i)
    glsl.insert(i.key() + offset, i.value());

  if(!rootImpl()->shadersHasConservativeDepth())
    {
      glsl.remove(preparePattern(QStringLiteral("layout \\( depth_any \\) ")));
      glsl.remove(preparePattern(QStringLiteral("layout \\( depth_greater \\) ")));
      glsl.remove(preparePattern(QStringLiteral("layout \\( depth_less \\) ")));
      glsl.remove(preparePattern(QStringLiteral("layout \\( depth_unchanged \\) ")));
      glsl.remove(preparePattern(QStringLiteral("out float gl_FragDepth ;")));
    }

  if(!rootImpl()->hasSeparateShaders())
    {
      if(!shader->shaderType().testFlag(QOpenGLShader::Vertex))
        {
          static QRegularExpression inExp = preparePattern(QStringLiteral("layout \\( location = \\d+ \\) in "));
          auto match = inExp.match(glsl);
          while(match.hasMatch())
            {
              glsl.replace(match.capturedStart(), match.capturedLength(), QStringLiteral("in "));
              match = inExp.match(glsl);
            }
        }

      if(!shader->shaderType().testFlag(QOpenGLShader::Fragment))
        {
          static QRegularExpression outExp = preparePattern(QStringLiteral("layout \\( location = \\d+ \\) out "));
          auto match = outExp.match(glsl);
          while(match.hasMatch())
            {
              glsl.replace(match.capturedStart(), match.capturedLength(), QStringLiteral("out "));
              match = outExp.match(glsl);
            }
        }
    }

  static QRegularExpression exp("GLOBAL_UBO_BINDING_(\\d+)");
  auto globalUboBindingMatch = exp.match(glsl);
  while(globalUboBindingMatch.hasMatch())
    {
      glsl.replace(globalUboBindingMatch.capturedStart(), globalUboBindingMatch.capturedLength(),
                   QString::number(globalUboBindingMatch.captured(1).toUInt() + KawaiiShader::getUboCount() + 1));
      globalUboBindingMatch = exp.match(glsl);
    }

  if(!rootImpl()->shadersHas420Pack())
    {
      requiredUboLocations.clear();
      static QRegularExpression exp = preparePattern("layout \\( std140, binding = (\\d+) \\) uniform (\\w+)");
      auto match = exp.match(glsl);
      while(match.hasMatch())
        {
          const GLuint uboBinding = match.captured(1).toUInt();
          const QString uboName = match.captured(2);

          glsl.replace(match.capturedStart(), match.capturedLength(), QStringLiteral("layout(std140) uniform %1").arg(uboName));
          requiredUboLocations[uboName] = uboBinding;
          match = exp.match(glsl);
        }
    }

  auto terms = glsl.split(';', sib_utils::strings::SplitBehaviorFlags::SkipEmptyParts);
  for(auto i = terms.begin(); i != terms.end(); )
    {
      if(Q_UNLIKELY(i->trimmed().isEmpty()))
        i = terms.erase(i);
      else
        ++i;
    }
  glsl = terms.join(';');

  if(!rootImpl()->isOpenGLES())
    glsl.remove(QRegularExpression(QLatin1String("precision[^;]+;")));

  return glsl;
}

SarielRootImpl *SarielShaderImpl::rootImpl()
{
  return static_cast<SarielRootImpl*>(getRoot());
}

void SarielShaderImpl::createShaderWork()
{
  shader = std::make_unique<QOpenGLShader>(trShaderType(modelObj->getType()), this);
  recompileWork();
}

void SarielShaderImpl::deleteShaderWork()
{
  shader.reset();
}

void SarielShaderImpl::recompileWork()
{
  if(!shader)
    return createShaderWork();

  code = preprocessGlsl(modelObj->getCode());
  shader->compileSourceCode(code);
  printLog();
}

void SarielShaderImpl::printLog()
{
  if(!shader->isCompiled())
    {
      qCritical() << "Problematic shader code:";
      qCritical().noquote() << shader->sourceCode();
      qCritical() << "::::::\r\nShader compile log:";
      qCritical().noquote() << shader->log();
      qCritical().quote();
    }
}
