#ifndef SARIELCAMERAIMPL_HPP
#define SARIELCAMERAIMPL_HPP

#include "SarielRenderer_global.hpp"
#include <KawaiiRenderer/KawaiiCameraImpl.hpp>

class SARIELRENDERER_SHARED_EXPORT SarielCameraImpl : public KawaiiCameraImpl
{
public:
  SarielCameraImpl(KawaiiCamera *model);

  // KawaiiCameraImpl interface
protected:
  inline void updateCache(DrawCache&, KawaiiGpuBufImpl*, const QRect&) override final {}
  DrawCache *createDrawCache(KawaiiGpuBufImpl *sfcUbo, const QRect &viewport) override final;
};

#endif // SARIELCAMERAIMPL_HPP
