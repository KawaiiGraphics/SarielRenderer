#ifndef SARIELBUFFERHANDLE_HPP
#define SARIELBUFFERHANDLE_HPP

#include <KawaiiRenderer/KawaiiBufferHandle.hpp>
#include "SarielRenderer_global.hpp"
#include <sib_utils/PairHash.hpp>
#include <QOpenGLBuffer>
#include <functional>
#include <memory>

class SarielRootImpl;

extern template class sib_utils::PairHash<GLenum, GLuint>;

class SARIELRENDERER_SHARED_EXPORT SarielBufferHandle: public KawaiiBufferHandle
{
public:
  SarielBufferHandle(const void *ptr, size_t n, SarielRootImpl &root);
  ~SarielBufferHandle();

  void setUboPostBindFunc(const std::function<void(GLenum, uint32_t)> &func);
  void setUboPostUnbindFunc(const std::function<void(GLenum, uint32_t)> &func);

  void setSubData(size_t offset, const void *ptr, size_t n);

  // KawaiiBufferHandle interface
  void setBufferData(const void *ptr, size_t n) override final;
  void sendChunkToGpu(size_t offset, size_t n) override final;
  size_t getBufferSize() const override final;
  void bind(KawaiiBufferTarget target) override final;
  void unbind(KawaiiBufferTarget target) override final;
  void bindToBlock(KawaiiBufferTarget target, uint32_t bindingPoint) override final;
  void userBinding(KawaiiBufferTarget target, uint32_t bindingPoint) override final;
  void unbindFromBlock(KawaiiBufferTarget target, uint32_t bindingPoint) override final;



  //IMPLEMENT
private:
  std::function<void(GLenum, uint32_t)> uboPostBind;
  std::function<void(GLenum, uint32_t)> uboPostUnbind;

  SarielRootImpl &r;
  std::unique_ptr<QOpenGLBuffer> buf;
  const void *data;
  size_t length;

  void setBufferDataWork(const void *ptr, size_t n);
  void setSubDataWork(size_t offset, const void *ptr, size_t n);
  void bindWork(KawaiiBufferTarget target);
  void unbindWork(KawaiiBufferTarget target);
  void bindToBlockWork(KawaiiBufferTarget target, uint32_t bindingPoint);
  void unbindFromBlockWork(KawaiiBufferTarget target, uint32_t bindingPoint);
};

#endif // SARIELBUFFERHANDLE_HPP
