#ifndef SARIELROOTIMPL_HPP
#define SARIELROOTIMPL_HPP

#include <KawaiiRenderer/KawaiiRootImpl.hpp>
#include "Textures/SarielTextureHandle.hpp"
#include "SarielBufferHandle.hpp"

#include <QPointer>
#include <QOpenGLContext>
#include <QOffscreenSurface>
#include <QOpenGLShaderProgram>
#include <QOpenGLExtraFunctions>

class SARIELRENDERER_SHARED_EXPORT SarielRootImpl: public KawaiiRootImpl
{
public:
  SarielRootImpl(KawaiiRoot *model);
  ~SarielRootImpl();

  void taskGL(const std::function<void()> &func);
  void asyncTaskGL(const std::function<void()> &func);

  QOpenGLExtraFunctions &gl() const;

  bool useShaderProg(QOpenGLShaderProgram &prog);

  float getGlFloat(GLenum pname);

  void makeCtxCurrent(QWindow *wnd);
  void startRendering(QWindow *wnd);
  void releaseTextures();
  void finishRendering(QWindow *wnd);

  void setBindedUbo(GLenum target, GLuint bindingPoint, void *buf);
  bool isUboBinded(void *buf) const;

  void bindTexture(const std::string &name, SarielTextureHandle *tex);
  void releaseTexture(SarielTextureHandle *tex);

  void resetUniformLocations(QOpenGLShaderProgram *prog);

  const QString& getGlslVersion() const;
  bool isOpenGLES() const;
  bool shadersHasConservativeDepth() const;
  bool hasSeparateShaders() const;
  bool shadersHas420Pack() const;

  static bool checkSystem();

  static QSurfaceFormat getPreferredSfcFormat();

  // KawaiiRootImpl interface
public:
  KawaiiBufferHandle *createBuffer(const void *ptr, size_t n, const std::initializer_list<KawaiiBufferTarget> &availableTargets) override final;
  KawaiiTextureHandle *createTexture(KawaiiTexture *model) override final;

private:
  void beginOffscreen() override final;
  void endOffscreen() override final;



  //IMPLEMENT
private:
  QOffscreenSurface sfcStub;
  QOpenGLContext glCtx;

  std::unordered_map<GLenum, GLfloat> glFloatParams;

  QOpenGLExtraFunctions *glFuncs;

  std::unordered_map<std::pair<GLenum, GLuint>, void*, sib_utils::PairHash<GLenum, GLuint>> bindedUbo;

  std::unordered_map<std::string, SarielTextureHandle*> textureNames;
  std::unordered_map<QOpenGLShaderProgram*, std::unordered_map<std::string, int>> uniformLocations;
  std::vector<SarielTextureHandle*> boundTextures;

  QPointer<QOpenGLShaderProgram> currentProg;

  QString glslVersion;
  bool isGles;
  bool glslHasConservativeDepth;
  bool glslHasSeparateShaders;
  bool glslHas420Pack;

  static void receiveGLDebug(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void*);


  Q_INVOKABLE void initContext();
  Q_INVOKABLE void deleteContext();

  std::unordered_map<std::string, int> &getUniformLocations(QOpenGLShaderProgram *prog);
  int getUniformLocation(QOpenGLShaderProgram *prog, const std::string &name);
  GLuint bindTexture(SarielTextureHandle *tex);
};

#endif // SARIELROOTIMPL_HPP
