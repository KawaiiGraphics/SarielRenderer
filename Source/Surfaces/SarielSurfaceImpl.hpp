#ifndef SARIELSURFACEIMPL_HPP
#define SARIELSURFACEIMPL_HPP

#include <KawaiiRenderer/Surfaces/KawaiiSurfaceImpl.hpp>
#include "../SarielRenderer_global.hpp"

#include <sib_utils/PairHash.hpp>
#include <QOpenGLPaintDevice>

class SarielRootImpl;
class SARIELRENDERER_SHARED_EXPORT SarielSurfaceImpl : public KawaiiSurfaceImpl
{
public:
  SarielSurfaceImpl(KawaiiSurface *model);

private:
  std::unique_ptr<QOpenGLPaintDevice> qpaint;
  std::unordered_map<std::pair<uint32_t, uint32_t>, float, sib_utils::PairHash<uint32_t, uint32_t>> cachedDepth;

  // KawaiiSurfaceImpl interface
  QPaintDevice* getPaintDevice() override final;
  void resize(const QSize &sz) override final;

  bool startRendering() override final;
  void prepareOverlayRendering() override final;
  void finishRendering() override final;

  inline void invalidate() override final {}

  float readDepth(float x, float y);
};

#endif // SARIELSURFACEIMPL_HPP
