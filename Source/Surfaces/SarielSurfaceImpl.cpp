#include "SarielSurfaceImpl.hpp"
#include "SarielRootImpl.hpp"
#include <Kawaii3D/KawaiiRoot.hpp>
#include <QOpenGLPaintDevice>

SarielSurfaceImpl::SarielSurfaceImpl(KawaiiSurface *model):
  KawaiiSurfaceImpl(model)
{
  const auto wndStates = getModel()->getWindow().windowStates();
  bool wasVisible = getModel()->getWindow().isVisible();

  auto *root = static_cast<SarielRootImpl*>(getModel()->getRoot()->getRendererImpl());
  getModel()->getWindow().setSurfaceType(QSurface::OpenGLSurface);
  getModel()->getWindow().setFormat(root->getPreferredSfcFormat());

  getModel()->getWindow().destroy();
  getModel()->getWindow().create();
  getModel()->getWindow().setWindowStates(wndStates);
  getModel()->getWindow().setVisible(wasVisible);
  getModel()->setReadDepthFunc([this] (float x, float y) { return readDepth(x, y); } );
  hasOverlayCache = false;
}

QPaintDevice *SarielSurfaceImpl::getPaintDevice()
{
  if(!qpaint)
    qpaint = std::make_unique<QOpenGLPaintDevice>(getModel()->getSize());
  return qpaint.get();
}

void SarielSurfaceImpl::resize(const QSize &sz)
{
  if(qpaint)
    qpaint->setSize(sz);
}

bool SarielSurfaceImpl::startRendering()
{
  auto *root = static_cast<SarielRootImpl*>(getModel()->getRoot()->getRendererImpl());
  root->startRendering(&getModel()->getWindow());
  root->gl().glBlendFunc(GL_ONE, GL_ZERO);
  if(!cachedDepth.empty())
    cachedDepth.clear();
  return true;
}

void SarielSurfaceImpl::prepareOverlayRendering()
{
  auto root = static_cast<SarielRootImpl*>(getModel()->getRoot()->getRendererImpl());
  root->gl().glEnable(GL_BLEND);
  root->gl().glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ZERO, GL_ONE);
  root->gl().glBindVertexArray(0);
  root->releaseTextures();
  root->gl().glActiveTexture(GL_TEXTURE0);
}

void SarielSurfaceImpl::finishRendering()
{
  static_cast<SarielRootImpl*>(getModel()->getRoot()->getRendererImpl())->finishRendering(&getModel()->getWindow());
  getModel()->getWindow().requestUpdate();
}

float SarielSurfaceImpl::readDepth(float x, float y)
{
  auto root = static_cast<SarielRootImpl*>(getModel()->getRoot()->getRendererImpl());
  root->makeCtxCurrent(&getModel()->getWindow());

  const uint32_t intX = std::trunc(x),
      intY = std::trunc(y);
  const std::pair<uint32_t, uint32_t> point(intX, intY);
  auto el = cachedDepth.find(point);

  if(el == cachedDepth.end())
    {
      float readed = std::numeric_limits<float>::quiet_NaN();
      root->gl().glReadPixels(std::trunc(x), getModel()->getWindow().height() - std::trunc(y) - 1, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &readed);
      if(!std::isnan(readed))
        {
          readed *= 2.0;
          readed -= 1.0;
        }
      if(!std::isnan(readed))
        el = cachedDepth.insert(std::pair(point, readed)).first;
      else
        return readed;
    }
  return el->second;
}
