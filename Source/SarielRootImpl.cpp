#include "SarielRootImpl.hpp"
#include "SarielBufferHandle.hpp"

#include <Kawaii3D/KawaiiConfig.hpp>
#include <QProcessEnvironment>
#include <QCoreApplication>
#include <QThread>
#include <iostream>

SarielRootImpl::SarielRootImpl(KawaiiRoot *model):
  KawaiiRootImpl(model),
  glCtx(this),
  glFuncs(nullptr),
  currentProg(nullptr),
  isGles(false),
  glslHasConservativeDepth(false),
  glslHasSeparateShaders(false),
  glslHas420Pack(false)
{
  moveToThread(QCoreApplication::instance()->thread());
  sfcStub.setParent(this);

  if(QThread::currentThread() == thread())
    initContext();
  else {
#     if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
      bool ok = QMetaObject::invokeMethod(this, &SarielRootImpl::initContext, Qt::BlockingQueuedConnection);
      Q_ASSERT(ok);
#     else
      throw std::runtime_error("Multithreading is not supported by MisakaRenderer on your version of Qt");
#     endif
    }
}

SarielRootImpl::~SarielRootImpl()
{
  if(QOpenGLContext::currentContext() == &glCtx)
    glCtx.doneCurrent();

  if(QThread::currentThread() == thread())
    deleteContext();
  else {
#     if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
      bool ok = QMetaObject::invokeMethod(this, &SarielRootImpl::deleteContext, Qt::BlockingQueuedConnection);
      Q_ASSERT(ok);
#     else
      throw std::runtime_error("Multithreading is not supported by MisakaRenderer on your version of Qt");
#     endif
    }
}

void SarielRootImpl::taskGL(const std::function<void ()> &func)
{
  if(QThread::currentThread() == thread())
    {
      if(QOpenGLContext::currentContext() != &glCtx)
        glCtx.makeCurrent(&sfcStub);
      func();
    } else
    {
#     if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
      bool ok = QMetaObject::invokeMethod(this, func, Qt::BlockingQueuedConnection);
      Q_ASSERT(ok);
#     else
      throw std::runtime_error("Multithreading is not supported by SarielRenderer on your version of Qt");
#     endif
    }
}

void SarielRootImpl::asyncTaskGL(const std::function<void ()> &func)
{
  if(QThread::currentThread() == thread())
    {
      if(QOpenGLContext::currentContext() != &glCtx)
        glCtx.makeCurrent(&sfcStub);
      func();
    } else
    {
#     if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
      bool ok = QMetaObject::invokeMethod(this, func, Qt::QueuedConnection);
      Q_ASSERT(ok);
#     else
      throw std::runtime_error("Multithreading is not supported by SarielRenderer on your version of Qt");
#     endif
    }
}

QOpenGLExtraFunctions &SarielRootImpl::gl() const
{
  return *glFuncs;
}

bool SarielRootImpl::useShaderProg(QOpenGLShaderProgram &prog)
{
  if(!prog.bind())
    return false;

  currentProg = &prog;

  for(auto &i: textureNames)
    if(int loc = getUniformLocation(currentProg, i.first); loc >= 0)
      currentProg->setUniformValue(loc, bindTexture(i.second));

  return true;
}

float SarielRootImpl::getGlFloat(GLenum pname)
{
  auto el = glFloatParams.find(pname);
  if(el == glFloatParams.end())
    {
      float result[4] = {0, 0, 0, 0};
      taskGL([this, pname, &result] {
          gl().glGetFloatv(pname, result);
        });
      el = glFloatParams.insert({pname, result[0]}).first;
    }
  return el->second;
}

void SarielRootImpl::makeCtxCurrent(QWindow *wnd)
{
  if(QOpenGLContext::currentContext() != &glCtx || glCtx.surface() != wnd)
    if(!glCtx.makeCurrent(wnd))
      qCritical("Can not make context current!");
}

void SarielRootImpl::startRendering(QWindow *wnd)
{
  makeCtxCurrent(wnd);
  endOffscreen();
  glFuncs->glEnable(GL_DEPTH_TEST);
  glFuncs->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void SarielRootImpl::releaseTextures()
{
  if(!boundTextures.empty())
    {
      for(size_t i = 0; i < boundTextures.size(); ++i)
        boundTextures[i]->getTexture().release(i);
      boundTextures.clear();
    }
}

void SarielRootImpl::finishRendering(QWindow *wnd)
{
  glCtx.swapBuffers(wnd);
  currentProg = nullptr;
}

void SarielRootImpl::setBindedUbo(GLenum target, GLuint bindingPoint, void *buf)
{
  bindedUbo[std::pair(target, bindingPoint)] = buf;
}

bool SarielRootImpl::isUboBinded(void *buf) const
{
  for(const auto &i: bindedUbo)
    if(i.second == buf)
      return true;
  return false;
}

void SarielRootImpl::bindTexture(const std::string &name, SarielTextureHandle *tex)
{
  textureNames[name] = tex;
  if(currentProg)
    if(int loc = getUniformLocation(currentProg, name); loc >= 0)
      currentProg->setUniformValue(loc, bindTexture(tex));
}

void SarielRootImpl::releaseTexture(SarielTextureHandle *tex)
{
  auto el = std::find(boundTextures.begin(), boundTextures.end(), tex);
  if(el != boundTextures.end())
    {
      GLuint unit = el - boundTextures.begin();
      tex->getTexture().release(unit);
      *el = nullptr;

      for(auto i = textureNames.cbegin(); i != textureNames.cend(); )
        {
          if(Q_UNLIKELY(i->second == tex))
            {
              if(currentProg)
                if(int loc = getUniformLocation(currentProg, i->first); loc >= 0)
                  currentProg->setUniformValue(loc, (GLuint)0);
              i = textureNames.erase(i);
            } else
            ++i;
        }
    }
}

void SarielRootImpl::resetUniformLocations(QOpenGLShaderProgram *prog)
{
  auto i = uniformLocations.find(prog);
  if(i != uniformLocations.end())
    uniformLocations.erase(i);
}

const QString &SarielRootImpl::getGlslVersion() const
{
  return glslVersion;
}

bool SarielRootImpl::isOpenGLES() const
{
  return isGles;
}

bool SarielRootImpl::shadersHasConservativeDepth() const
{
  return glslHasConservativeDepth;
}

bool SarielRootImpl::hasSeparateShaders() const
{
  return glslHasSeparateShaders;
}

bool SarielRootImpl::shadersHas420Pack() const
{
  return glslHas420Pack;
}

namespace {
  template<size_t N>
  constexpr QLatin1String latin1Str(const char (&str)[N])
  {
    return str[N-1] == '\0'? QLatin1String(str, N-1): QLatin1String(str, N);
  }
}

bool SarielRootImpl::checkSystem()
{
  const bool dbg_enabled = KawaiiConfig::getInstance().getDebugLevel() > 0;
  auto dbg_prnt = [&dbg_enabled] (const auto &str) {
      if(dbg_enabled)
        qWarning().noquote() << latin1Str(str);
    };

  const auto fmt = getPreferredSfcFormat();

  QOffscreenSurface sfc;
  sfc.setFormat(fmt);
  sfc.create();

  QOpenGLContext ctx;
  ctx.setFormat(fmt);

  if(!ctx.create() || !ctx.isValid())
    {
      dbg_prnt("SarielRenderer failed to init OpenGL context!");
      return false;
    }

  if(!ctx.makeCurrent(&sfc))
    {
      dbg_prnt("SarielRenderer failed to make OpenGL context current!");
      return false;
    }

  bool result = true;

  QOpenGLExtraFunctions *gl_func = ctx.extraFunctions();
  if(!gl_func)
    {
      dbg_prnt("SarielRenderer failed to init OpenGL functions!");
      result = false;
    }

  ctx.doneCurrent();
  sfc.destroy();

  return result;
}

QSurfaceFormat SarielRootImpl::getPreferredSfcFormat()
{
  QSurfaceFormat fmt = KawaiiConfig::getInstance().getPreferredSfcFormat();
  fmt.setStencilBufferSize(8);

  static const auto systemEnv = QProcessEnvironment::systemEnvironment();

  if(systemEnv.value("SIB3D_FORCE_OPENGL", "0").toInt())
    fmt.setRenderableType(QSurfaceFormat::OpenGL);
  else if(systemEnv.value("SIB3D_FORCE_COREGL", "0").toInt())
    {
      fmt.setRenderableType(QSurfaceFormat::OpenGL);
      fmt.setProfile(QSurfaceFormat::CoreProfile);
      fmt.setVersion(3,3);
    }
  else if(systemEnv.value("SIB3D_FORCE_GLES", "0").toInt())
    fmt.setRenderableType(QSurfaceFormat::OpenGLES);

  fmt.setOption(QSurfaceFormat::DeprecatedFunctions, false);
  return fmt;
}

KawaiiBufferHandle *SarielRootImpl::createBuffer(const void *ptr, size_t n, const std::initializer_list<KawaiiBufferTarget> &availableTargets)
{
  static_cast<void>(availableTargets);
  return new SarielBufferHandle(ptr, n, *this);
}

KawaiiTextureHandle *SarielRootImpl::createTexture(KawaiiTexture *model)
{
  return new SarielTextureHandle(*this, model);
}

void SarielRootImpl::beginOffscreen()
{ }

void SarielRootImpl::endOffscreen()
{
  glFuncs->glBindFramebuffer(GL_FRAMEBUFFER, glCtx.defaultFramebufferObject());
  releaseTextures();
}

void SarielRootImpl::receiveGLDebug(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *)
{
  const uint16_t dbg = KawaiiConfig::getInstance().getDebugLevel();

  static const std::unordered_map<GLenum, QLatin1String> sourceNames = {
    {GL_DEBUG_SOURCE_API,             latin1Str("calls to the OpenGL API")},
    {GL_DEBUG_SOURCE_WINDOW_SYSTEM,   latin1Str("calls to a window-system API")},
    {GL_DEBUG_SOURCE_SHADER_COMPILER, latin1Str("compiler for a shading language")},
    {GL_DEBUG_SOURCE_THIRD_PARTY,     latin1Str("application associated with OpenGL")},
    {GL_DEBUG_SOURCE_APPLICATION,     latin1Str("user of this application")},
    {GL_DEBUG_SOURCE_OTHER,           latin1Str("Unknown")}
  };

  static const std::unordered_map<GLenum, QLatin1String> typeNames = {
    {GL_DEBUG_TYPE_ERROR,               latin1Str("Error")},
    {GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR, latin1Str("Warning (deprecated)")},
    {GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR,  latin1Str("Warning (undefined behavior)")},
    {GL_DEBUG_TYPE_PORTABILITY,         latin1Str("Warning (functionality is not portable)")},
    {GL_DEBUG_TYPE_PERFORMANCE,         latin1Str("Warning (performance issues)")},
    {GL_DEBUG_TYPE_MARKER,              latin1Str("Command stream annotation")},
    {GL_DEBUG_TYPE_PUSH_GROUP,          latin1Str("Group pushed")},
    {GL_DEBUG_TYPE_POP_GROUP,           latin1Str("Group poped")},
    {GL_DEBUG_TYPE_OTHER,               latin1Str("Unknown")}
  };

  if(length < 0 || strlen(reinterpret_cast<const char*>(message)) < static_cast<size_t>(length))
    return;

  switch(severity)
    {
    case GL_DEBUG_SEVERITY_HIGH:
      qCritical().setAutoInsertSpaces(true);
      qCritical() << "OpenGL debug message #" << id << '{';
      qCritical() << "\tFrom " << sourceNames.at(source);
      qCritical() << "\tType: " << typeNames.at(type);
      qCritical() << "\tText: " << reinterpret_cast<const char*>(message);
      qCritical().nospace() << '}';
      break;

    case GL_DEBUG_SEVERITY_MEDIUM:
      if(dbg < 2) break;
      qWarning() << "OpenGL debug message #" << id << '{';
      qWarning() << "\tFrom " << sourceNames.at(source);
      qWarning() << "\tType: " << typeNames.at(type);
      qWarning() << "\tText: " << reinterpret_cast<const char*>(message);
      qWarning().nospace() << '}';
      break;

    default:
      if(dbg < 3) break;
      qInfo() << "OpenGL debug message #" << id << '{';
      qInfo() << "\tFrom " << sourceNames.at(source);
      qInfo() << "\tType: " << typeNames.at(type);
      qInfo() << "\tText: " << reinterpret_cast<const char*>(message);
      qInfo().nospace() << '}';
      break;
    }
}

void SarielRootImpl::initContext()
{
  const auto fmt = getPreferredSfcFormat();
  sfcStub.setFormat(fmt);
  sfcStub.create();

  glCtx.setFormat(fmt);
  glCtx.create();

  glCtx.makeCurrent(&sfcStub);

  glFuncs = glCtx.extraFunctions();
  glFuncs->glEnable(GL_DEPTH_TEST);
  glFuncs->glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

  bool hasGlDebug = false;

  const auto extensions = glCtx.extensions();

  if(glCtx.format().renderableType() == QSurfaceFormat::OpenGLES)
    {
      isGles = true;
      glslVersion = QStringLiteral("#version 320 es\n"
                                   "#extension GL_EXT_separate_shader_objects: enable\n"
                                   "#extension GL_EXT_texture_cube_map_array: enable\n"
                                   "#extension GL_EXT_shader_implicit_conversions: enable\n"
                                   "#extension GL_OES_depth_texture: enable\n"
                                   "#extension GL_OES_depth_texture_cube_map: enable\n"
                                   "precision mediump samplerCubeArrayShadow;\n"
                                   "precision mediump sampler2DArrayShadow;\n"
                                   "precision mediump float; precision mediump int;\n"
                                   );
      glslHasConservativeDepth = false;
      glslHasSeparateShaders = true;
      hasGlDebug = true;
    } else
    {
      isGles = false;
      glslVersion = QStringLiteral("#version 140\n"
                                   "#undef lowp\n"
                                   "#undef mediump\n"
                                   "#undef highp\n"
                                   "#extension GL_ARB_uniform_buffer_object: enable\n"
                                   "#extension GL_ARB_explicit_attrib_location: enable\n"
                                   "#extension GL_ARB_texture_cube_map_array: enable\n"
                                   );

      if(extensions.contains("GL_ARB_conservative_depth"))
        {
          glslVersion += QStringLiteral("#extension GL_ARB_conservative_depth: enable\n");
          glslHasConservativeDepth = true;
        }

      if(extensions.contains("GL_ARB_separate_shader_objects"))
        {
          glslVersion += QStringLiteral("#extension GL_ARB_separate_shader_objects: enable\n");
          glslHasSeparateShaders = true;
        }

      if(extensions.contains("GL_ARB_shading_language_420pack"))
        {
          glslVersion += QStringLiteral("#extension GL_ARB_shading_language_420pack: enable\n");
          glslHas420Pack = true;
        }

      if(extensions.contains("GL_KHR_debug") || extensions.contains("GL_ARB_debug_output"))
        hasGlDebug = true;
    }

  if(hasGlDebug && KawaiiConfig::getInstance().getDebugLevel() > 0)
    {
      glFuncs->glEnable(GL_DEBUG_OUTPUT);
      glFuncs->glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
      glFuncs->glDebugMessageCallback(&SarielRootImpl::receiveGLDebug, nullptr);
    }

  glFuncs->glClearColor(0,0,0, 1.0);
  qDebug("SarielRenderer: Using OpenGL version \'%s\'", reinterpret_cast<const char*>(glFuncs->glGetString(GL_VERSION)));
  qDebug().noquote() << latin1Str("SarielRenderer: OpenGL extensions: ") << extensions.values();
}

void SarielRootImpl::deleteContext()
{
  currentProg.clear();
  textureNames.clear();
  glCtx.doneCurrent();
  sfcStub.destroy();
}

std::unordered_map<std::string, int> &SarielRootImpl::getUniformLocations(QOpenGLShaderProgram *prog)
{
  auto el = uniformLocations.find(prog);
  if(el == uniformLocations.end())
    {
      connect(prog, &QOpenGLShaderProgram::destroyed, this, [this, prog] { resetUniformLocations(prog); });
      el = uniformLocations.insert({prog, std::unordered_map<std::string, int>()}).first;
    }
  return el->second;
}

int SarielRootImpl::getUniformLocation(QOpenGLShaderProgram *prog, const std::string &name)
{
  auto &cache = getUniformLocations(prog);
  auto el = cache.find(name);
  if(el == cache.end())
    {
      int loc = prog->uniformLocation(name.c_str());
      el = cache.insert({name, loc}).first;
    }
  return el->second;
}

GLuint SarielRootImpl::bindTexture(SarielTextureHandle *tex)
{
  auto el = std::find(boundTextures.cbegin(), boundTextures.cend(), tex);
  if(el != boundTextures.cend())
    return el - boundTextures.cbegin();

  GLuint unit;
  auto nullEl = std::find(boundTextures.begin(), boundTextures.end(), nullptr);
  if(nullEl != boundTextures.end())
    {
      *nullEl = tex;
      unit = nullEl - boundTextures.cbegin();
    } else
    {
      unit = boundTextures.size();
      boundTextures.push_back(tex);
    }
  tex->getTexture().bind(unit);
  return unit;
}
